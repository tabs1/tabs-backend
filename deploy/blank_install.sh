#!/bin/bash -ex
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update -y && sudo apt install yarn -y && sudo apt install -y awscli
sudo apt install -y postgresql && sudo -u postgres createdb tabs
sudo apt install -y python3-pip
sudo apt-get install -y python-psycopg2
sudo apt-get install -y libpq-dev
sudo apt install -y python3-flask

echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"' | sudo -u postgres psql -d tabs
echo "ALTER USER postgres PASSWORD 'newpassword';" | sudo -u postgres psql
export DATABASE_URL=postgresql://postgres:newpassword@localhost/tabs
export FLASK_APP=launch.py
cd application/frontend
yarn install
yarn build
cd ../..
flask db upgrade
flask run -h 0.0.0.0 -p 5000
