Parameters:
  Ec2ImageId:
    Type: String
    Default: ami-b987c3d8
    Description: Enter the image id of the ec2 instance type to be used. This comes from the catalog. Default is ami-0d8f6eb4f641ef691.
  Ec2KeyPairName:
    Type: String
    Default: crucible-key
    Description: Enter the name of the ec2 key pair to be used for ssh. This is created on the ec2 section of the console. Default is fett-3-infrastructure.
  S3ResourceParamter:
    Type: String
    Default: 'arn:aws-us-east-1:s3:::'
    Description: Enter the arn for s3. Default is 'arn:aws-us-gov:s3:::'
  TelerathS3BucketName:
    Type: String
    Default: tabs-data
    Description: Enter a name for the s3 bucket. Default is telerath-data.
  TelerathEC2SecurityGroupName:
    Type: String
    Default: tabs
    Description: Enter a name for the security group to be applied to the Telerath EC2 instance.
  AvailabilityZone1:
    Type: String
    Default: us-east-1
    Description: >-
      Enter an availability zone different from Availability Zone 2. Default is
      us-gov-west-1a.
  AvailabilityZone2:
    Type: String
    Default: us-gov-west-1b
    Description: >-
      Enter an availability zone different from Availability Zone 1. Default is
      us-gov-west-1b.
  TelerathResourcesBucketName:
    Type: String
    Description: The name of the bucket that hosts the latest telerath release tar ball
    Default: telerath-resources

Resources:
  TelerathInternetGateway:
    Type: AWS::EC2::InternetGateway
  TelerathGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref TelerathInternetGateway
      VpcId: !Ref TelerathVPC
  TelerathPublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref TelerathVPC
  TelerathPublicRoute:
    Type: 'AWS::EC2::Route'
    DependsOn: TelerathInternetGateway
    Properties:
      RouteTableId: !Ref TelerathPublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref TelerathInternetGateway
  TelerathSubnetRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref TelerathPublicSubnet
      RouteTableId: !Ref TelerathPublicRouteTable
  TelerathPublicSubnet:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref TelerathVPC
      CidrBlock: 10.192.10.0/24
      MapPublicIpOnLaunch: true
  TelerathAppNode:
    Type: AWS::EC2::Instance
    Properties:
      IamInstanceProfile: !Ref TelerathEc2InstanceProfile
      SubnetId: !Ref TelerathPublicSubnet
      SecurityGroupIds:
        - !Ref TelerathAppNodeSG
      InstanceType: t2.small
      ImageId: !Ref Ec2ImageId
      KeyName: !Ref Ec2KeyPairName
      UserData: !Base64 |
        #!/bin/bash -ex
        curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
        echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
        sudo apt update -y && sudo apt install yarn -y && sudo apt install -y awscli
        sudo apt install -y postgresql && sudo -u postgres createdb telerath_c2
        sudo apt install -y python3-pip
        sudo apt-get install -y python-psycopg2
        sudo apt-get install -y libpq-dev
        sudo apt install -y python3-flask
        aws s3 cp s3://telerath-resources/telerath-resource.tar.gz /home/ubuntu/ --region=us-gov-west-1
        cd home/ubuntu/
        tar xzf telerath-resource.tar.gz
        pip3 install -r installation/dependencies.txt
        echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"' | sudo -u postgres psql -d telerath_c2
        echo "ALTER USER postgres PASSWORD 'newpassword';" | sudo -u postgres psql
        export DATABASE_URL=postgresql://postgres:newpassword@localhost/telerath_c2
        export FLASK_APP=launch.py
        cd application/frontend
        yarn install
        yarn build
        cd ../..
        flask db upgrade
        flask run -h 0.0.0.0 -p 5000
        /opt/aws/bin/cfn-signal --exit-code 0 --resource TelerathAppNode --region ${AWS::Region} --stack ${AWS::StackName}
  TelerathAppNodeSG:
    Type: AWS::EC2::SecurityGroup
    CreationPolicy:
      ResourceSignal:
        Timeout: PT5M
    Properties:
      VpcId: !Ref TelerathVPC
      GroupDescription: security group for the EC2 instance running the app
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '443'
          ToPort: '443'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '5000'
          ToPort: '5000'
          CidrIp: 0.0.0.0/0
  TelerathVPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 10.192.0.0/16
      EnableDnsSupport: true
      EnableDnsHostnames: true
  TelerathEc2Role:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      ManagedPolicyArns:
        - !Ref Ec2ReadBucketPolicy
        - !Ref Ec2WriteBucketPolicy
  Ec2ReadBucketPolicy:
    Type: 'AWS::IAM::ManagedPolicy'
    Properties:
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
        - Effect: Allow
          Action: [ 's3:GetObject', 's3:ListBucket', 's3:ListAllMyBuckets' ]
          Resource: "*"
  Ec2WriteBucketPolicy:
    Type: 'AWS::IAM::ManagedPolicy'
    Properties:
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
        - Effect: Allow
          Action: [ 's3:PutObject' ]
          Resource: "*"
  TelerathEc2InstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Path: '/'
      Roles:
        - !Ref TelerathEc2Role
