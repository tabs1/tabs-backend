from seeds.main import dbseed

def start_seeding():
    dbseed()

if __name__ == "__main__":
    start_seeding()