# TABS Training Tracking System

TODO: replace with gitlab-ci file TABS

- Provides an interface to interact with database storing information about training status
- Shows metrics in easily digestable dashboards to give commanders visibility on training in their units
- Allows unit members to view the status of their training and update their chain of command when training is completed.

# [Installation](https://gitlab.com/stevewillson/tabs-backend/-/blob/master/installation/README.md)

# [Testing](https://gitlab.com/stevewillson/tabs-backend/-/blob/master/tests/README.md)

# API Endpoints

`flask routes`

## Base Object APIs

### `GET /[object_name]/<uuid>`
Returns an object selected by its UUID. Verifies that a user has permission to view a certain object.

#### Payload

None (GET REQUEST)

#### Response

If successful, the endpoint returns a json object that represents the object
stored under that UUID. The object will have the following structure:

```json
{
    "created_at":"2019-09-05T23:05:07",
    "uuid":"e1cf8726-ef62-454d-a06c-ba92663baaa3"
}
```

### `POST /[object_name]/`

Creates an object from the data in the request. Must be a POST and user must be authorized to create objects of this type.


#### Payload

```json
{
    "object_prop_1" : "prop1_val",
    "object_prop_2" : 69.42,
    "object_prop_3" : ["some list"]
}
```

#### Response

Returns the object that was created which should have an automatically generated uuid and timestamp when returned. 

```json
{
    "created_at":"2019-09-05T23:05:07",
    "uuid":"e1cf8726-ef62-454d-a06c-ba92663baaa3",
    "object_prop_1" : "prop1_val",
    "object_prop_2" : 69.42,
    "object_prop_3" : ["some list"]
}
```

If there are errors in creating the object, the dictionary will contain an error designator with an error message with the following structure:

```json
{
    "error" : "Object not created"
}
```

### `POST /[object_name]/<uuid>`

This function updates an existing object.

#### Payload
```json
{
    "object_prop_1" : "prop1_val",
    "object_prop_2" : 69.42,
    "object_prop_3" : ["some list"]
}
```


#### Response
```json
{
    "created_at":"2019-09-05T23:05:07",
    "uuid":"e1cf8726-ef62-454d-a06c-ba92663baaa3",
    "object_prop_1" : "prop1_val",
    "object_prop_2" : 69.42,
    "object_prop_3" : ["some list"]
}
```

### `/[object_name]s`

#### Payload

Filter and pagination designators

#### Response

```json
{
    "Current Page": 1,
    "Pages": 2,
    "Resource Count": 11,
    "Resources Per Page": 10,
    "members": [
        {
            "adcon_control": null,
            "basd": null,
            "created_at": "2020-03-18T10:12:43.507445",
            "date_of_rank": null,
            "ets_date": null,
            "first_name": "Loser",
            "groups": [],
            "last_four": "0443",
            "last_name": "Winner",
            "middle_name": "Test",
            "mos": "17A",
            "opcon_control": null,
            "pay_grade_code": "O-2",
            "pcs_date": null,
            "rank_short": "1LT",
            "smos": "17D",
            "trainings": [],
            "uuid": "7a4241a9-016a-468c-b41a-f631dd64de76"
        },
        {
            "adcon_control": null,
            "basd": null,
            "created_at": "2020-03-18T10:12:43.527757",
            "date_of_rank": null,
            "ets_date": null,
            "first_name": "Loser",
            "groups": [],
            "last_four": "0443",
            "last_name": "Winner",
            "middle_name": "Test",
            "mos": "17A",
            "opcon_control": null,
            "pay_grade_code": "O-2",
            "pcs_date": null,
            "rank_short": "1LT",
            "smos": "17D",
            "trainings": [],
            "uuid": "04dcb8a2-1236-4ad7-8834-c3c1e42dcfb2"
        }
    ]
}
```


