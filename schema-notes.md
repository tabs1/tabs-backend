# Preliminary notes on database schema

## Table Captures
### used to log observation of a device
    id Primary Key
    source_device_id FK references device_capture.id
    destination_device_id NULLABLE FK references device_capture.id
    packet_type VarChar enumerable
    signal_strength int
    ssid VarChar nullable
    captured_at Datetime
    latitude Float
    longitude Float
    location_id FK references location.id*

## Table Devices
### used to identify an observed device
    id Primary Key
    Mac_Address Varchar


## Table Locations
### (TBD) used for isolating geocodes to more precise location (ie address/city/region)
    id Primary Key
    TBD
