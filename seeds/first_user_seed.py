from application import *
import json

def seed():
    if app.config['ENV'] != 'testing':
        print('Seeding First User ...')

        data_user = {
            'username' : 'Jango-Fett',
            'password' : 'PleaseChangeThis--10',
            'is_active' : True,
            'group_admin' : True,
            'write_privileges' : True,
            'email_address' : 'example@dds.mil'
        }

        data_role_admin = {
            'name' : 'Admin',
        }

        data_role_basic = {
            'name' : 'Basic',
        }

        data_units = [
            {
                'name' : 'A Co',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
            {
                'name' : 'B Co',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
            {
                'name' : 'C Co',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
            {
                'name' : 'D Co',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
            {
                'name' : 'E Co',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
            {
                'name' : 'HHC Co',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
            {
                'name' : '782nd MI BN',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : False,
                'highest_echelon' : False 
            },
            {
                'name' : '780th MI BDE',
                'poc' : 'test@gmail.com',
                'lowest_echelon' : True,
                'highest_echelon' : False 
            },
        ]

        member_data = {
            'last_name' : 'Winner',
            'middle_name' : 'Test',
            'first_name' : 'Loser',
            'rank_short' : '1LT',
            'last_four' : '0443',
            'pay_grade_code' : 'O-2',
            'mos' : '17A',
            'smos' : '17D',
        }     
        seed_units(data_units, member_data)

        user = User.query.filter_by(username = data_user['username']).first()
        if user:
            status = 'Record Found'
            user_service = UserService(user)
        else:
            status = 'Record Created'
            user_service = UserService().create(data_user)
            role_service = RoleService().create(data_role_admin)
            role_service2 = RoleService().create(data_role_basic)
            user_service.append('roles', role_service)
            user_service.append('roles', role_service2)

        if user_service.errors:
            print(json.dumps({'errors' : user_service.errors}, indent=1))
        else:
            print(status)

        print(json.dumps(user_service.simple_response(), indent=1))

def seed_units(units_array, member_data):
    for unit in units_array:
        unit_service =  UnitService().create(unit)
        packet, code = unit_service.simple_response()
        for i in range (0,10):
            member_data_local = member_data
            member_data_local['unit'] = packet['uuid']
            member_service = MemberService().create(member_data_local)
