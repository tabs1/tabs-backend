from os.path import dirname, basename, isfile, join
import glob
import importlib


def dbseed():

    print("Planting Seeds ...")

    for f in glob.glob(join(dirname("seeds/"), "*seed.py")):
        if isfile(f) and not f.endswith('__init__.py'):
            seed = f.split("/")[1].split(".")[0]
            module = importlib.import_module("seeds." + seed, package=None)
            module.seed()

    print("Finished Seeding ...")



