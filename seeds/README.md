# Seeds - Intro

This is data that is required for the app to run (ie configurations). These should be written 
in a way that is idempotent, and their existance is tested before inserting (get_or_create is 
PERFECT for this) allowing for the entire seed file to always run without issue.

# Running
- Seeds are configured in the `/seeds`
- Run: `flask dbseed`
- This calls a script in cli.py which will include and run all seeds in `/seeds`


# Example 
A `configurations` table that needs some pre-populated data for the application to run

## configurations_seed.py

```
from application.services import *

def seed():

    print("Seeding Configurations ...")
    configurations = [
        { 'name' : 'campaign_key', 'value' : 'campaign_secret' }
    ]

    for configuration in configurations:
        configuration_service = ConfigurationService().get_or_create(configuration);
        if configuration_service.instance:
            print(str(configuration_service.instance.id) + " : " + configuration_service.instance.name)
        else:
            print('---> error adding ' + configuration['name'])
            print(configuration_service.simple_response())

```
