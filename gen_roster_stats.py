import csv
import pandas as pd


def main():
    df = pd.read_csv("~/Documents/anasoc_master_tracker.csv")
    print(df.dtypes)
    target = df
    for col in target.columns:
        counts = target[col].value_counts()
        counts.to_csv("~/Documents/anasoc_tracker_stats/" + col + "_Count.csv")




if __name__ == '__main__':
    main()
