from flask import abort, request, session
from application import db
from .certificate import *
from sqlalchemy import func
from application.models.user_model import User

def authorize_user():
    if 'user_id' in session:
        return

    # Log in valiation starts here
    email = _get_email_from_cert()
    user = _get_authenticated_user(email)
    if user is None:
        abort(403)

    # User officially logs in here
    session['user_id'] = user.id
    session['email_address'] = user.email_address
    session['role'] = user.role.name if user.role else None

def _get_authenticated_user(email):
    user = None
    if email:
        try:
            user = User.query.filter(
                func.lower(User.email_address) == email
            ).first()
        except:
            user = None
    return user

def _get_email_from_cert():
    email = None
    try:
        cert_pem = request.environ.get('HTTPS_CC')
        if cert_pem:
            cert = Certificate(cert_pem.encode())
            email = cert.get_email().lower()
    except Exception as e:
        print(e)
        email = None
    return email
