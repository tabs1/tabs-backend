from cryptography import x509
from cryptography.hazmat.backends import default_backend

class Certificate:

    def __init__(self, cert_bytes):
        self.cert = x509.load_pem_x509_certificate(cert_bytes, default_backend())

    def get_email(self):
        try:
            ext = self.cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
            emails = ext.value.get_values_for_type(x509.RFC822Name)
            if emails:
                return emails[0]
            else:
                raise ValueError("No email in subjectAltName available for certificate")
        except x509.extensions.ExtensionNotFound:
            raise ValueError("No subjectAltName available for certificate")
