from config import Config
import logging

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import create_engine

app = Flask(__name__, static_folder='frontend/build/')
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
create_engine(db.engine.url)


# Logging
logger = logging.getLogger()
if app.config['ENV'] == 'development':
    logger.setLevel(logging.DEBUG)
    app
elif app.config['ENV'] == 'testing':
    logger.setLevel(logging.WARN)

# Order Matters!
from application.models import *
from application.serializers import *
from application.services import *
from application.controllers import *
from cli import *
