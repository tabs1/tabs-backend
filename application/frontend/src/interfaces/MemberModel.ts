import { TrainingModel } from "./TrainingModel";
import { GroupModel } from "./GroupModel";

export interface MemberModel {
    // Relationships
    trainings : TrainingModel[]
    group : string // Note this is a notional relationship implemented in 

    // Attributes
    last_name : string
    middle_name : string
    first_name : string
    rank_short : string
    last_four : string
    pay_grade_code : string
    mos : string
    smos : string
    date_of_rank : string
    basd : string
    ets_date : string
    pcs_date : string
    opcon_control : string
    adcon_control : string

}

export const emptyMember: MemberModel = {
    // Relationships
    trainings : [],
    group : '',

    // Attributes
    last_name : '',
    middle_name : '',
    first_name : '',
    rank_short : '',
    last_four : '',
    pay_grade_code : '',
    mos : '',
    smos : '',
    date_of_rank : '',
    basd : '',
    ets_date : '',
    pcs_date : '',
    opcon_control : '',
    adcon_control : '',

}
