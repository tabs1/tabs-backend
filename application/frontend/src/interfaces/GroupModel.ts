import { MemberModel } from './MemberModel'
import { TrainingModel } from './TrainingModel';

export interface GroupModel {
	uuid: string
	created_at: string
	name: string
    role: string
    members: MemberModel[]
    trainings: TrainingModel[]
}
