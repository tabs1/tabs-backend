import { MemberModel } from './MemberModel'
import { UserModel } from './UserModel'

export interface UnitModel {
	uuid: string
	created_at: string
	name: string
    poc : string
    members: MemberModel[]
    users: UserModel[]
}


export const emptyUnitModel : UnitModel = {
	uuid: '',
	created_at: '',
	name: '',
    poc : '',
    members: [],
    users: [],
}
