import { MemberModel } from "./MemberModel"

export interface TrainingModel {
    members : MemberModel[]
    group_id : string
    task_number : string
    task_name : string
    frequency : string
}

export const emptyTraining: TrainingModel = {
	// default empty strings, 0, and false for all values
	members: [],
	group_id: '',
	task_name: '',
    task_number:'',
    frequency: '',
}
