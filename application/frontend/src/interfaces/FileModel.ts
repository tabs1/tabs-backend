export interface FileModel {
    uuid: string
	created_at: string
    name : string
    bucket : string
    type : string 
    derog : boolean
    description : string
    classification : string
    dissemination : string
    
    user : string
    member : string
    case : string
}


