import { UnitModel } from './UnitModel'

export interface UserModel {
    units : UnitModel[]

	created_at: string
	is_active: boolean
    group_admin : boolean
    group_uuid: string
    write_privileges : boolean
	username: string
	uuid: string
}

export const emptyUserModel : UserModel = {
    units : [],

	created_at: '',
	is_active: false,
    group_admin : false,
    group_uuid: '',
    write_privileges : false,
	username: '',
	uuid: '',
}
