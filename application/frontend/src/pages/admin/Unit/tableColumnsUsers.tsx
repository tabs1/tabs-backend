import React from 'react'

import TETableLink from 'components/TETableLink'
import TETableButton from 'components/TETableButton'

type RemoveUser = (user_uuid: string, unit_uuid: string) => void
export const tableColumnsUsers = (removeUser: RemoveUser, unitUUID: string) => [
    {
		id: 'username',
		Header: 'Name',
		accessor: 'name',
	},
    {
		id: 'created_at',
		Header: 'Created On',
		accessor: 'created_at',
	},
	{
		id: 'details',
		Header: '',
		width: 100,
		sortable: false,
		filterable: false,
		className: 'actionCell',
		accessor: 'uuid',
		Cell: (d: { value: string }) => (
			<>
				<TETableLink to={`/admin/group/${d.value}`}>Details</TETableLink>
				<TETableButton onClick={() => removeUser(unitUUID, d.value)}>Unassign</TETableButton>
			</>
		),
	},
]
