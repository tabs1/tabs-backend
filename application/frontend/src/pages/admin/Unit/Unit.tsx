import React, { useState, useEffect, ChangeEvent } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import {
	TEErrorLoadingAlert,
	TEPanelWrapper,
	TEPanel,
	TEForm,
	TEInputRow,
	TEButton,
	useTEPopups,
    TELabel,
    TESearchSelectInput,
} from 'react-tec'

import { tableColumnsUsers } from './tableColumnsUsers'

import TEReactTable from 'components/TEReactTable'
import { UserModel, UnitModel } from 'interfaces'

import { useBarTitle, useAppContext } from 'hooks'

import { useUnit } from './hooks'
import { saveUnit, confirmRemoveUser } from './requests'

interface Props extends RouteComponentProps<{ uid: string }> {}
const Unit = (props: Props) => {
	useBarTitle('Unit')
	const { match } = props
	const { uid } = match.params
	const popupFunctions = useTEPopups()
	const { userToken } = useAppContext()
	const [unit, setUnit] = useState<UnitModel>()
	const { unitOrig, unitLoaded, loadUnit, userArray } = useUnit(uid)

	useEffect(() => {
		if(unitOrig) {
			setUnit(unitOrig)
		}
	}, [unitOrig])
	

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		try {
			const data = {
				uid,
				unit,
				userToken,
				popupFunctions,
			}
			await saveUnit(data)
			loadUnit()
		} catch (e) {
			console.log(e)
		}
	}

    const handleRemoveUser = (unit_uuid : string, user_uuid : string) => {
        const data = {
            user_uuid: user_uuid,
            unit_uuid: unit_uuid,
            userToken,
            popupFunctions,
            loadUnit,
        }
        confirmRemoveUser(data)
    }

	if (!unit) {
		if (unitLoaded) {
			return (
				<TEErrorLoadingAlert
					title='Error Loading'
					message='Error occured while loading Device Data.'
				/>
			)
		} else {
			return null //Still Loading
		}
	}

	return (
		<TEPanelWrapper>
			<TEPanel title='Unit'>
				<TEForm onSubmit={handleSubmit}>
					<TEInputRow
						labelForKey='name'
						title='Unit Name'
						value={unit.name}
						onChange={(e: ChangeEvent<HTMLInputElement>) => {
							setUnit({ ...unit, name: e.target.value})
						}}
					/>
					<TEInputRow
						labelForKey='poc'
						title='Unit POC'
						value={unit.poc}
						onChange={(e: ChangeEvent<HTMLInputElement>) => {
							setUnit({ ...unit, poc: e.target.value})
						}}
					/>
                    <TELabel>Users Assigned</TELabel>
                    <TESearchSelectInput
                        type='multi'
                        title='Users Assigned'
						labelForKey='users'
						onChange={({ options }: { options: UserModel[] }) => {
							setUnit({ ...unit, users : options})
						}}
                        options={userArray}
                        value={unit.users}
                        optionLabelPath='username'
                    />
					<TEButton type='submit'>Save Unit</TEButton>
				</TEForm>
			</TEPanel>
            <TEPanel title='Users'>
				<TEReactTable
					data={unit.users || []}
					columns={tableColumnsUsers(handleRemoveUser, uid)}
					filterable
					defaultSorted={[
						{
							id: 'dateCreated',
							desc: false,
						},
					]}
				/>
			</TEPanel>
		</TEPanelWrapper>
	)
}

export default Unit
