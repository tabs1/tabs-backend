import validate from 'validate.js'

import { apiRequest } from 'helpers'
import { useTEPopupsFunctions } from 'react-tec'
import { UnitModel } from 'interfaces'

interface Data {
	uid: string
	unit : UnitModel | undefined
	userToken: string | undefined | null
	popupFunctions: useTEPopupsFunctions
}
export const saveUnit = async (data: Data) => {
	const { uid, unit, userToken, popupFunctions } = data
	const { showAlert, showNetworkActivity, hideNetworkActivity } = popupFunctions

	//Validate Data
	const validatorConstraints = {
		unit: {
			presence: {
				allowEmpty: false,
			},
		},
	}
	const validationResponse = validate(data, validatorConstraints)
	if (validationResponse) {
		const valuesArray: any[][] = Object.values(validationResponse)
		const firstError: any[] = valuesArray[0]
		const firstErrorMessage: string = firstError[0]
		showAlert({
			title: 'Error',
			message: firstErrorMessage,
		})
		throw new Error(firstErrorMessage)
	}

	try {
		showNetworkActivity('Updating Unit...')
		const data = {
			...unit,
		}
		await apiRequest({
			method: 'POST',
			headers: { Authorization: userToken },
			path: `unit/${uid}`,
			data,
		})
		hideNetworkActivity()
		showAlert({
			title: 'Success',
			message: 'Unit Updated.',
		})
		return
	} catch (e) {
		//Handle Errors
		if (e && e.response && e.response.data && e.response.data.errors) {
			console.log(e.response.data.errors)
			hideNetworkActivity()
			for (const key in e.response.data.errors) {
				const message = e.response.data.errors[key][0]
				showAlert({
					title: 'Error',
					message: `${key} - ${message}`,
				})
				throw new Error(`${key} - ${message}`)
			}
		}
		hideNetworkActivity()
		showAlert({
			title: 'Error',
			message: e.message,
		})
		throw new Error(e)
	}
}

interface RemoveUserData {
	user_uuid: string
	unit_uuid: string
	userToken: string | null | undefined
	popupFunctions: useTEPopupsFunctions
	loadUnit(): void
}
export const confirmRemoveUser = (data: RemoveUserData) => {
	const { user_uuid, unit_uuid, userToken, popupFunctions, loadUnit } = data
	const {
		showNetworkActivity,
		hideNetworkActivity,
		showAlert,
		showConfirm,
		hideConfirm,
	} = popupFunctions

	const removeUser = async () => {
		try {
			hideConfirm()
			showNetworkActivity('Unassigning User...')
			const data = {
				user_uuid: user_uuid,
				unit_uuid: unit_uuid,
			}
			await apiRequest({
				method: 'DELETE',
				headers: { Authorization: userToken },
				path: `unit/user/assign`,
				data,
			})
			loadUnit()
			hideNetworkActivity()
			showAlert({
				title: 'Success',
				message: 'User unassigned.',
			})
			return
		} catch (e) {
			//Handle Errors
			if (e && e.response && e.response.data && e.response.data.errors) {
				console.log(e.response.data.errors)
				hideNetworkActivity()
				for (const key in e.response.data.errors) {
					const message = e.response.data.errors[key][0]
					showAlert({
						title: 'Error',
						message: `${key} - ${message}`,
					})
					throw new Error(`${key} - ${message}`)
				}
			}
			hideNetworkActivity()
			showAlert({
				title: 'Error',
				message: 'Error attempting to unassign user.',
			})
			return
		}
	}

	showConfirm({
		title: 'Unassign User',
		message: 'Are you sure you want to unassign this user?',
		leftTitle: 'Cancel',
		rightOnClick: removeUser,
		rightTitle: 'Unassign User',
	})
}
