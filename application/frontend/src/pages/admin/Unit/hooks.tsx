import { useState, useEffect, useCallback } from 'react'

import { apiRequest } from 'helpers'
import { useAppContext } from 'hooks'
import { UnitModel, UserModel } from 'interfaces'

export const useUnit = (uid: string) => {
	const { userToken } = useAppContext()

	const [unit, setUnit] = useState<UnitModel | undefined>(undefined)
	const [unitLoaded, setUnitLoaded] = useState(false)
    const [userArray, setUserArray] = useState<UserModel[]>([])


	const loadUnit = useCallback(() => {
		const loadUnit = async () => {
			try {
				const response: { data: UnitModel } = await apiRequest({
					method: 'GET',
					headers: { Authorization: userToken },
					path: `unit/${uid}`,
				})
				setUnit(response && response.data)
				setUnitLoaded(true)
			} catch (e) {
				console.log(e)
				setUnit(undefined)
				setUnitLoaded(true)
			}
		}
		loadUnit()
	}, [uid, userToken])

	useEffect(() => {
		loadUnit()

		return () => {
			setUnit(undefined)
			setUnitLoaded(false)
		}
	}, [uid, userToken, loadUnit])

    const loadUsers = useCallback(() => {
        const loadUsers = async () => {
            try {
                const response: { data: { users: UserModel[] } } = await apiRequest({
                    method: 'GET',
                    headers: { Authorization: userToken },
                    path: `users_no_pages`,
                })

                const { users = [] } = response && response.data
                setUserArray(users)
            } catch (e) {
                console.log(e)
                setUserArray([])
            }
        }
        loadUsers()
    }, [userToken])

    useEffect(() => {
        loadUsers()
        return () => {
            setUserArray([])
        }
    }, [userToken, loadUsers])

	return { unitOrig : unit, unitLoaded, loadUnit, userArray }
}
