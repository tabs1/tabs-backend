import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import {
	TEErrorLoadingAlert,
	TEPanelWrapper,
	TEPanel,
	TEForm,
	TEInputRow,
	TEButton,
	useTEPopups,
} from 'react-tec'

import TEPanelActionButton from 'components/TEPanelActionButton'
import TEPanelActionWrapper from 'components/TEPanelActionWrapper'
import TEReactTable from 'components/TEReactTable'

import { useBarTitle, useAppContext } from 'hooks'
import { convertToDateShort } from 'helpers'

import { useMember } from './hooks'
import { saveMember, advanceMember } from './requests'
import { MemberModel } from 'interfaces'

// TODO: Clean up all these unused things.

interface Props extends RouteComponentProps<{ uid: string }> {}
const Member = (props: Props) => {
	const { match } = props
	const { uid } = match.params
    const popupFunctions = useTEPopups()
	const [editingActive, setEditingActive] = useState(false)
	useBarTitle('Member')
	const { userToken } = useAppContext()

    const [member, setMember] = useState<MemberModel>()

	const {
		memberOrig,
		memberLoaded,
		loadMember,
	} = useMember(uid)

	useEffect(() => {
		if(memberOrig) {
			setMember(memberOrig)
		}
	}, [memberOrig])

    const handleSaveMember = async (e: FormEvent) => {
		try {
			e.preventDefault()
			const data = {
                popupFunctions,
                userToken,
				member,
				uid,
			}
			await saveMember(data)
			loadMember()
			setEditingActive(false)
		} catch (e) {
			console.log(e)
		}
	}


	if (!member) {
		if (memberLoaded) {
			return (
				<TEErrorLoadingAlert
					title='Error Loading'
					message='Error occured while loading Member Data.'
				/>
			)
		} else {
			return null //Still Loading
		}
	}
    return (
        <>
            <TEPanelWrapper>
                <TEPanel
                    title='Summary'
                    rightComponent={
                        <TEPanelActionWrapper>
                            <TEPanelActionButton onClick={() => setEditingActive(true)}>
                                Edit Member
                            </TEPanelActionButton>
                        </TEPanelActionWrapper>
                    }
                >
    				<TEReactTable
    					data={[member]}
    					columns={[
    						{
    							id: 'given_name',
    							Header: 'Given Name',
    							accessor: 'given_name',
    						},
    						{
    							id: 'created_at',
    							Header: 'Date Created',
    							accessor: 'created_at',
    							Cell: (d: any) => convertToDateShort(d.value),
    						},
    						{
    							id: 'ana_id',
    							Header: 'ANA ID',
    							accessor: 'ana_id',
    						},
    						{
    							id: 'unit',
    							Header: 'Unit ',
    							accessor: 'unit',
    						},
    						{
    							id: 'cell_number_1',
    							Header: 'Cell Number',
    							accessor: 'cell_number_1',
    						},
    					]}
    					sortable={false}
    					filterable={false}
    					showPagination={false}
    					defaultPageSize={1}
    				/>
    			</TEPanel>
				{(!editingActive) && ( 
    			<TEPanel title='Basic Information' subtitle='Current Status' size='half'>
    				<p>First Name: {member.first_name}</p>
					<p>Last Name: {member.last_name}</p>
    				<p>Rank: {member.rank_short}</p>
    				<p>MOS: {member.mos}</p>
    			</TEPanel>
				)}
				{(editingActive) && ( 
					<TEPanel title='Details' size='half'>
						<TEForm onSubmit={handleSaveMember}>
							<TEInputRow
								labelForKey='first_name'
								title='First Name'
								value={member.first_name}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									setMember({ ...member, first_name: e.target.value })
								}}
								required
							/>
							<TEInputRow
								labelForKey='last_name'
								title='Last Name'
								value={member.last_name}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									setMember({ ...member, last_name: e.target.value })
								}}
								required
							/>
							<TEInputRow
								labelForKey='rank_short'
								title='Rank'
								value={member.rank_short}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									setMember({ ...member, rank_short: e.target.value })
								}}
								required
							/>
							<TEInputRow
								labelForKey='mos'
								title='MOS'
								value={member.mos}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									setMember({ ...member, mos: e.target.value })
								}}
								required
							/>
							<TEInputRow
								labelForKey='smos'
								title='SMOS'
								value={member.smos}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									setMember({ ...member, smos: e.target.value })
								}}
								required
							/>
							<TEInputRow
								labelForKey='pay_grade_code'
								title='Pay Grade'
								value={member.pay_grade_code}
								onChange={(e: ChangeEvent<HTMLInputElement>) => {
									setMember({ ...member, pay_grade_code: e.target.value })
								}}
								required
							/>
							<TEButton type='submit'>Update Member</TEButton>
						</TEForm>
				</TEPanel>
				)}
    		</TEPanelWrapper>
        </>
    )
}

export default Member
