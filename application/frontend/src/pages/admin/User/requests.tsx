import validate from 'validate.js'
import { useTEPopupsFunctions } from 'react-tec'

import { apiRequest } from 'helpers'
import { UserModel } from 'interfaces'

interface Data {
	uid: string
	user: UserModel | undefined
	userToken: string | undefined | null
	popupFunctions: useTEPopupsFunctions
}
export const saveUser = async (data: Data) => {
	const { uid, user, userToken, popupFunctions } = data
	const { showAlert, showNetworkActivity, hideNetworkActivity } = popupFunctions

	//Validate Data
	const validatorConstraints = {
		user: {
			presence: {
				allowEmpty: false,
			},
		},
	}
	const validationResponse = validate(data, validatorConstraints)
	if (validationResponse) {
		const valuesArray: any[][] = Object.values(validationResponse)
		const firstError: any[] = valuesArray[0]
		const firstErrorMessage: string = firstError[0]
		showAlert({
			title: 'Error',
			message: firstErrorMessage,
		})
		throw new Error(firstErrorMessage)
	}

	try {
		showNetworkActivity('Updating User...')
		const data = {
			...user,
			relationships: {
				units: user? user.units : [],
			}
		}
		await apiRequest({
			method: 'POST',
			headers: { Authorization: userToken },
			path: `user/${uid}`,
			data,
		})
		hideNetworkActivity()
		showAlert({
			title: 'Success',
			message: 'User Updated.',
		})
		return
	} catch (e) {
		//Handle Errors
		if (e && e.response && e.response.data && e.response.data.errors) {
			console.log(e.response.data.errors)
			hideNetworkActivity()
			for (const key in e.response.data.errors) {
				const message = e.response.data.errors[key][0]
				showAlert({
					title: 'Error',
					message: `${key} - ${message}`,
				})
				throw new Error(`${key} - ${message}`)
			}
		}
		hideNetworkActivity()
		showAlert({
			title: 'Error',
			message: e.message,
		})
		throw new Error(e)
	}
}

interface RemoveUnitData {
	user_uuid: string
	unit_uuid: string
	userToken: string | null | undefined
	popupFunctions: useTEPopupsFunctions
	loadUser(): void
}
export const confirmRemoveUnit = (data: RemoveUnitData) => {
	const { user_uuid, unit_uuid, userToken, popupFunctions, loadUser } = data
	const {
		showNetworkActivity,
		hideNetworkActivity,
		showAlert,
		showConfirm,
		hideConfirm,
	} = popupFunctions

	const removeUnit = async () => {
		try {
			hideConfirm()
			showNetworkActivity('Unassigning Unit...')
			const data = {
				unit_uuid,
				user_uuid
			}
			await apiRequest({
				method: 'DELETE',
				headers: { Authorization: userToken },
				path: `current_user/unit/assign`,
				data,
			})
			loadUser()
			hideNetworkActivity()
			showAlert({
				title: 'Success',
				message: 'Unit unassigned.',
			})
			return
		} catch (e) {
			//Handle Errors
			if (e && e.response && e.response.data && e.response.data.errors) {
				console.log(e.response.data.errors)
				hideNetworkActivity()
				for (const key in e.response.data.errors) {
					const message = e.response.data.errors[key][0]
					showAlert({
						title: 'Error',
						message: `${key} - ${message}`,
					})
					throw new Error(`${key} - ${message}`)
				}
			}
			hideNetworkActivity()
			showAlert({
				title: 'Error',
				message: 'Error attempting to unassign unit.',
			})
			return
		}
	}

	showConfirm({
		title: 'Unassign Unit',
		message: 'Are you sure you want to unassign this unit?',
		leftTitle: 'Cancel',
		rightOnClick: removeUnit,
		rightTitle: 'Unassign Unit',
	})
}
