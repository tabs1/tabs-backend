import React, { useState, useEffect, ChangeEvent } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { UnitModel, UserModel } from 'interfaces'
import {
	TEErrorLoadingAlert,
	TEPanelWrapper,
	TEPanel,
	TEForm,
	TEInputRow,
    TELabel,
	TESegmentedGroup,
	TEButton,
	useTEPopups,
	TESearchSelectInput,
} from 'react-tec'

import { useBarTitle, useAppContext } from 'hooks'
import TEReactTable from 'components/TEReactTable'

import { useUser } from './hooks'
import { saveUser, confirmRemoveUnit } from './requests'

import { tableColumns } from './tableColumns'

interface Props
	extends RouteComponentProps<{
		uid: string
	}> {}

const User = (props: Props) => {
	const { match } = props
	const { uid } = match.params
	useBarTitle('User')

	const popupFunctions = useTEPopups()
	const { userToken } = useAppContext()
	const [user, setUser] = useState<UserModel>()

	const {
		userOrig,
		userLoaded,
		loadUser,
		unitArray,
	} = useUser(uid)

	useEffect(() => {
		if(userOrig) {
			setUser(userOrig)
		}
	}, [userOrig])

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		try {
			const data = {
				uid,
				user,
				userToken,
				popupFunctions,
			}
			await saveUser(data)
			loadUser()
		} catch (e) {
			console.log(e)
		}
	}
	const handleRemoveUnit = (unit_uuid: string) => {
		const data = {
			user_uuid: uid,
			unit_uuid: unit_uuid,
			userToken,
			popupFunctions,
			loadUser,
		}
		confirmRemoveUnit(data)
	}

	if (!user) {
		if (userLoaded) {
			return (
				<TEErrorLoadingAlert
					title='Error Loading'
					message='Error occured while loading Device Data.'
				/>
			)
		} else {
			return null //Still Loading
		}
	}

	return (
		<TEPanelWrapper>
			<TEPanel title='User'>
				<TEForm onSubmit={handleSubmit}>
					<TEInputRow
						labelForKey='username'
						title='Username'
						value={user.username}
						onChange={() => {}}
						disabled
					/>
					<TESegmentedGroup
						labelForKey='is_active'
						title='Account Active'
						checkedValue={user.is_active ? 'Yes' : 'No'}
						onChange={(e: ChangeEvent<HTMLInputElement>) => {
							setUser({ ...user, is_active: e.target.value === 'Yes'})
						}}
						buttonArray={['Yes', 'No']}
						required
						inline
					/>
					<TESegmentedGroup
						labelForKey='write_privileges'
						title='Write Privileges'
						checkedValue={user.write_privileges ? 'Yes' : 'No'}
						onChange={(e: ChangeEvent<HTMLInputElement>) => {
							setUser({ ...user, write_privileges: e.target.value === 'Yes'})
						}}
						buttonArray={['Yes', 'No']}
						required
						inline
					/>
					<TESegmentedGroup
						labelForKey='group_admin'
						title='Group Admin'
						checkedValue={user.group_admin ? 'Yes' : 'No'}
						onChange={(e: ChangeEvent<HTMLInputElement>) => {
							setUser({ ...user, group_admin: e.target.value === 'Yes'})
						}}
						buttonArray={['Yes', 'No']}
						required
						inline
					/>
                    <TELabel>Units Assigned</TELabel>
                    <TESearchSelectInput
						type='multi'
						title='Units'
						labelForKey='units'
						onChange={({ options }: { options: UnitModel[] }) => {
							setUser({ ...user, units : options})
						}}
						options={unitArray}
						value={user.units}
						optionLabelPath='name'
					/>
					<TEButton type='submit'>Save User</TEButton>
				</TEForm>
			</TEPanel>
			<TEPanel title='Units'>
				<TEReactTable
					data={user.units || []}
					columns={tableColumns(handleRemoveUnit)}
					filterable
					defaultSorted={[
						{
							id: 'dateCreated',
							desc: false,
						},
					]}
				/>
			</TEPanel>
		</TEPanelWrapper>
	)
}

export default User
