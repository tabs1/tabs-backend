import React from 'react'

import TETableLink from 'components/TETableLink'
import TETableButton from 'components/TETableButton'

type RemoveUnit = (unit_uuid: string) => void
export const tableColumns = (removeUnit: RemoveUnit) => [
	{
		id: 'name',
		Header: 'Name',
		accessor: 'name',
	},
	{
		id: 'role',
		Header: 'Role',
		accessor: 'role',
	},
	{
		id: 'details',
		Header: '',
		width: 100,
		sortable: false,
		filterable: false,
		className: 'actionCell',
		accessor: 'uuid',
		Cell: (d: { value: string }) => (
			<>
				<TETableLink to={`/admin/unit/${d.value}`}>Details</TETableLink>
				<TETableButton onClick={() => removeUnit(d.value)}>Unassign</TETableButton>
			</>
		),
	},
]
