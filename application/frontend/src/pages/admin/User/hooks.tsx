import { useState, useEffect, useCallback } from 'react'

import { apiRequest } from 'helpers'
import { useAppContext } from 'hooks'
import { UserModel, UnitModel } from 'interfaces'

export const useUser = (uid: string) => {
	const { userToken } = useAppContext()
	const [user, setUser] = useState<UserModel | undefined>(undefined)
	const [userLoaded, setUserLoaded] = useState(false)
	const [unitArray, setUnitArray] = useState<UnitModel[]>([])

	const loadUser = useCallback(() => {
		const loadUser = async () => {
			console.log(uid)
			try {
				const response: any = await apiRequest({
					method: 'GET',
					headers: { Authorization: userToken },
					path: `user/${uid}`,
				})
				console.log(response && response.data)
				setUser(response && response.data)
				setUserLoaded(true)
			} catch (e) {
				console.log(e)
				setUser(undefined)
				setUserLoaded(true)
			}
		}
		loadUser()
	}, [uid, userToken])

	useEffect(() => {
		loadUser()

		return () => {
			setUser(undefined)
			setUserLoaded(false)
		}
	}, [uid, userToken, loadUser])

	const loadUnits = useCallback(() => {
		const loadUnits = async () => {
			try {
				const response: { data: { units: UnitModel[] } } = await apiRequest({
					method: 'GET',
					headers: { Authorization: userToken },
					path: `units_no_pages`,
				})
				console.log(response)
				const { units = [] } = response && response.data
				setUnitArray(units)
			} catch (e) {
				console.log(e)
				setUnitArray([])
			}
		}
		loadUnits()
	}, [userToken])

	useEffect(() => {
		loadUnits()
		return () => {
			setUnitArray([])
		}
	}, [userToken, loadUnits])

	return { userOrig:user, userLoaded, loadUser, unitArray }
}
