import React from 'react'

import TETableLink from 'components/TETableLink'

import { filterDateColumn, convertToDateShort } from 'helpers'
import TETableButton from 'components/TETableButton'

export const tableColumns = (createCase: (memberUuid: string) => void) => [
	{
		id: 'last_name',
		Header: 'Last Name',
		accessor: 'last_name',
	},
	{
		id: 'first_name',
		Header: 'First Name',
		accessor: 'first_name',
	},
	{
		id: 'created_at',
		Header: 'Date Created',
		filterMethod: (filter: any, row: any) => filterDateColumn(convertToDateShort, filter, row),
		accessor: 'created_at',
		Cell: (d: any) => convertToDateShort(d.value),
	},
	{
		id: 'mos',
		Header: 'MOS',
		accessor: 'mos',
	},
	{
		id: 'unit',
		Header: 'Unit ',
		accessor: 'unit',
	},
	{
		id: 'actions',
		Header: '',
		width: 100,
		sortable: false,
		filterable: false,
		className: 'actionCell',
		accessor: 'uuid',
		Cell: (d: any) => (
			<>
				<TETableLink to={`/admin/member/${d.value}`}>Details</TETableLink>
			</>
		),
	},
]
