import React, { useState, ChangeEvent } from 'react'
import { TEPopupForm, TEInputRow, useTEPopups } from 'react-tec'

import TEPopupTitle from 'components/TEPopupTitle'

import { useAppContext } from 'hooks'

import { addMember } from './requests'
import { MemberModel, emptyMember } from 'interfaces'

interface Props {
	visible: boolean
	onSubmit(): void
	onClose(): void
	loadData(): void
}
const AddMemberPopup = (props: Props) => {
	const { visible, onSubmit, onClose, loadData } = props

	const { userToken } = useAppContext()
	const popupFunctions = useTEPopups()
	const [member, setMember] = useState<MemberModel>(emptyMember)

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		try {
			const data = {
				member,
				userToken,
				popupFunctions,
			}
			await addMember(data)
			onSubmit()
			loadData()
		} catch (e) {
			console.log(e)
		}
	}

	return (
		<TEPopupForm visible={visible} onClose={onClose} onSubmit={handleSubmit}>
			<TEPopupTitle>Add Member</TEPopupTitle>
				<TEInputRow
					labelForKey='first_name'
					title='First Name'
					value={member.first_name}
					onChange={(e: ChangeEvent<HTMLInputElement>) => {
						setMember({ ...member, first_name: e.target.value })
					}}
					required
				/>
				<TEInputRow
					labelForKey='last_name'
					title='Last Name'
					value={member.last_name}
					onChange={(e: ChangeEvent<HTMLInputElement>) => {
						setMember({ ...member, last_name: e.target.value })
					}}
					required
				/>
				<TEInputRow
					labelForKey='rank_short'
					title='Rank'
					value={member.rank_short}
					onChange={(e: ChangeEvent<HTMLInputElement>) => {
						setMember({ ...member, rank_short: e.target.value })
					}}
					required
				/>
				<TEInputRow
					labelForKey='mos'
					title='MOS'
					value={member.mos}
					onChange={(e: ChangeEvent<HTMLInputElement>) => {
						setMember({ ...member, mos: e.target.value })
					}}
					required
				/>
				<TEInputRow
					labelForKey='smos'
					title='SMOS'
					value={member.smos}
					onChange={(e: ChangeEvent<HTMLInputElement>) => {
						setMember({ ...member, smos: e.target.value })
					}}
					required
				/>
				<TEInputRow
					labelForKey='pay_grade_code'
					title='Pay Grade'
					value={member.pay_grade_code}
					onChange={(e: ChangeEvent<HTMLInputElement>) => {
						setMember({ ...member, pay_grade_code: e.target.value })
					}}
					required
				/>
		</TEPopupForm>
	)
}

export default AddMemberPopup
