import React, { useState, useEffect, ChangeEvent, FormEvent } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import {
	TEErrorLoadingAlert,
	TEPanelWrapper,
	TEPanel,
	TEForm,
	TEInputRow,
	TEButton,
	useTEPopups,
} from 'react-tec'

import TEPanelActionButton from 'components/TEPanelActionButton'
import TEPanelActionWrapper from 'components/TEPanelActionWrapper'
import TEReactTable from 'components/TEReactTable'

import { useBarTitle, useAppContext } from 'hooks'
import { convertToDateShort } from 'helpers'

import { useTraining } from './hooks'
import { saveTraining, advanceTraining } from './requests'
import { TrainingModel } from 'interfaces'

// TODO: Clean up all these unused things.

interface Props extends RouteComponentProps<{ uid: string }> {}
const Training = (props: Props) => {
	const { match } = props
	const { uid } = match.params
    const popupFunctions = useTEPopups()
	const [editingActive, setEditingActive] = useState(false)
	useBarTitle('Training')
	const { userToken } = useAppContext()

    const [training, setTraining] = useState<TrainingModel>()

	const {
		trainingOrig,
		trainingLoaded,
		loadTraining,
	} = useTraining(uid)

	useEffect(() => {
		if(trainingOrig) {
			setTraining(trainingOrig)
		}
	}, [trainingOrig])

    const handleSaveTraining = async (e: FormEvent) => {
		try {
			e.preventDefault()
			const data = {
                popupFunctions,
                userToken,
				training,
				uid,
			}
			await saveTraining(data)
			loadTraining()
			setEditingActive(false)
		} catch (e) {
			console.log(e)
		}
	}


	if (!training) {
		if (trainingLoaded) {
			return (
				<TEErrorLoadingAlert
					title='Error Loading'
					message='Error occured while loading Training Data.'
				/>
			)
		} else {
			return null //Still Loading
		}
	}
    return (
        <>
            <TEPanelWrapper>
                <TEPanel
                    title='Summary'
                    rightComponent={
                        <TEPanelActionWrapper>
                            <TEPanelActionButton onClick={() => setEditingActive(true)}>
                                Edit Training
                            </TEPanelActionButton>
                        </TEPanelActionWrapper>
                    }
                >
    				<TEReactTable
    					data={[training]}
    					columns={[
    						{
    							id: 'given_name',
    							Header: 'Given Name',
    							accessor: 'given_name',
    						},
    						{
    							id: 'created_at',
    							Header: 'Date Created',
    							accessor: 'created_at',
    							Cell: (d: any) => convertToDateShort(d.value),
    						},
    						{
    							id: 'ana_id',
    							Header: 'ANA ID',
    							accessor: 'ana_id',
    						},
    						{
    							id: 'unit',
    							Header: 'Unit ',
    							accessor: 'unit',
    						},
    						{
    							id: 'cell_number_1',
    							Header: 'Cell Number',
    							accessor: 'cell_number_1',
    						},
    					]}
    					sortable={false}
    					filterable={false}
    					showPagination={false}
    					defaultPageSize={1}
    				/>
    			</TEPanel>
				{(!editingActive) && ( 
    			<TEPanel title='Basic Information' subtitle='Current Status' size='half'>
    				<p>Training Name: {training.task_name}</p>
					<p>Training ID: {training.task_number}</p>
    				<p>Frequency (days): {training.frequency}</p>
    			</TEPanel>
				)}
				{(editingActive) && ( 
					<TEPanel title='Details' size='half'>
						<TEForm onSubmit={handleSaveTraining}>
									<TEInputRow
										labelForKey='task_name'
										title='Training Name'
										value={training.task_name}
										onChange={(e: ChangeEvent<HTMLInputElement>) => {
											setTraining({ ...training, task_name: e.target.value })
										}}
										required
									/>
									<TEInputRow
										labelForKey='task_number'
										title='Training ID'
										value={training.task_number}
										onChange={(e: ChangeEvent<HTMLInputElement>) => {
											setTraining({ ...training, task_number: e.target.value })
										}}
										required
									/>
									<TEInputRow
										labelForKey='frequency'
										title='Frequency'
										value={training.frequency}
										onChange={(e: ChangeEvent<HTMLInputElement>) => {
											setTraining({ ...training, frequency: e.target.value })
										}}
										required
									/>
									
									<TEButton type='submit'>Update Training</TEButton>
								</TEForm>
						</TEPanel>
				)}
    		</TEPanelWrapper>
        </>
    )
}

export default Training
