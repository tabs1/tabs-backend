import { apiRequest } from 'helpers'
import { useTEPopupsFunctions } from 'react-tec'

import {
	TrainingModel
} from 'interfaces'

interface Data {
	training : TrainingModel | undefined
	userToken : string | null | undefined
	uid : string
	popupFunctions: useTEPopupsFunctions
}
export const saveTraining = async (data: Data) => {
	const {
		uid,
		userToken,
		training,
		popupFunctions,
	} = data
	const { showAlert, showNetworkActivity, hideNetworkActivity } = popupFunctions

	try { 
		showNetworkActivity('Updating Training...')
		const data = { training }
		await apiRequest({
			method: 'POST',
			headers: { Authorization: userToken },
			path: `training/${uid}`,
			data,
		})
		hideNetworkActivity()
		showAlert({
			title: 'Success',
			message: 'Training Updated.',
		})
		return
	} catch (e) {
		//Handle Errors
		if (e && e.response && e.response.data && e.response.data.errors) {
			console.log(e.response.data.errors)
			hideNetworkActivity()
			for (const key in e.response.data.errors) {
				const message = e.response.data.errors[key][0]
				showAlert({
					title: 'Error',
					message: `${key} - ${message}`,
				})
				throw new Error(`${key} - ${message}`)
			}
		}
		hideNetworkActivity()
		showAlert({
			title: 'Error',
			message: e.message,
		})
		throw new Error(e)
	}
}

export const advanceTraining = async (uid : string, userToken : string | undefined | null, popupFunctions: useTEPopupsFunctions) => {
    const { showAlert, showNetworkActivity, hideNetworkActivity } = popupFunctions
    try {
        showNetworkActivity('Advancing Training...')
        await apiRequest({
            method: 'POST',
            headers: { Authorization: userToken },
            path: `training/advance/${uid}`,
        })
    } catch (e) {
		//Handle Errors
		if (e && e.response && e.response.data && e.response.data.errors) {
			console.log(e.response.data.errors)
			hideNetworkActivity()
			for (const key in e.response.data.errors) {
				const message = e.response.data.errors[key][0]
				showAlert({
					title: 'Error',
					message: `${key} - ${message}`,
				})
				throw new Error(`${key} - ${message}`)
			}
		}
		hideNetworkActivity()
		showAlert({
			title: 'Error',
			message: e.message,
		})
		throw new Error(e)
	}
}
