import { useState, useEffect, useCallback } from 'react'

import { apiRequest } from 'helpers'
import { useAppContext } from 'hooks'
import { TrainingModel } from 'interfaces'

export const useTraining = (uid: string) => {
	const { userToken } = useAppContext()

	const [training, setTraining] = useState<TrainingModel | undefined>(undefined)
	const [trainingLoaded, setTrainingLoaded] = useState(false)

	const loadTraining = useCallback(() => {
		const loadTraining = async () => {
			try {
				const response: { data: TrainingModel } = await apiRequest({
					method: 'GET',
					headers: { Authorization: userToken },
					path: `training/${uid}`,
				})
				setTraining(response && response.data)
				setTrainingLoaded(true)
			} catch (e) {
				console.log(e)
				setTraining(undefined)
				setTrainingLoaded(true)
			}
		}
		loadTraining()
	}, [uid, userToken])
	useEffect(() => {
		loadTraining()

		return () => {
			setTraining(undefined)
			setTrainingLoaded(false)
		}
	}, [uid, userToken, loadTraining])

	return { trainingOrig: training, trainingLoaded, loadTraining }
}
