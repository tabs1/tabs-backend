import { useState, useEffect, useCallback } from 'react'

import { apiRequest } from 'helpers'
import { useAppContext } from 'hooks'
import { GroupModel, UserModel, MemberModel } from 'interfaces'

export const useGroup = (uid: string) => {
	const { userToken } = useAppContext()

	const [groupOrig, setGroupOrig] = useState<GroupModel | undefined>(undefined)
	const [groupLoaded, setGroupLoaded] = useState(false)
    const [memberArray, setMemberArray] = useState<MemberModel[]>([])
    const [membersLoaded, setMembersLoaded] = useState(false)

	const loadGroup = useCallback(() => {
		const loadGroup = async () => {
			try {
				const response: { data: GroupModel } = await apiRequest({
					method: 'GET',
					headers: { Authorization: userToken },
					path: `group/${uid}`,
				})
				setGroupOrig(response && response.data)
				setGroupLoaded(true)
			} catch (e) {
				console.log(e)
				setGroupOrig(undefined)
				setGroupLoaded(true)
			}
		}
		loadGroup()
	}, [uid, userToken])
	useEffect(() => {
		loadGroup()

		return () => {
			setGroupOrig(undefined)
			setGroupLoaded(false)
		}
	}, [uid, userToken, loadGroup])

    const loadMembers = useCallback(() => {
        const loadMembers = async () => {
            try {
                const response: { data: { members: MemberModel[] } } = await apiRequest({
                    method: 'GET',
                    headers: { Authorization: userToken },
                    path: `members_no_pages`,
                })

                const { members = [] } = response && response.data
                setMemberArray(members)
                setMembersLoaded(true)
            } catch (e) {
                console.log(e)
                setMemberArray([])
                setMembersLoaded(true)
            }
        }
        loadMembers()
    }, [userToken])

    useEffect(() => {
        loadMembers()
        return () => {
            setMemberArray([])
        }
    }, [userToken, loadMembers])

	return { groupOrig, groupLoaded, memberArray, membersLoaded, loadGroup }
}
