import React from 'react'

import TETableLink from 'components/TETableLink'
import TETableButton from 'components/TETableButton'
import { filterDateColumn, convertToDateShort } from 'helpers'

type RemoveMember = (uuid: string , unit_uuid: string) => void
export const tableColumnsMembers = (removeMember: RemoveMember | undefined, groupUUID: string) => [
    {
		id: 'last_name',
		Header: 'Last Name',
		accessor: 'last_name',
	},
	{
		id: 'first_name',
		Header: 'First Name',
		accessor: 'first_name',
	},
	{
		id: 'created_at',
		Header: 'Date Created',
		filterMethod: (filter: any, row: any) => filterDateColumn(convertToDateShort, filter, row),
		accessor: 'created_at',
		Cell: (d: any) => convertToDateShort(d.value),
	},
	{
		id: 'mos',
		Header: 'MOS',
		accessor: 'mos',
	},
	{
		id: 'unit',
		Header: 'Unit ',
		accessor: 'unit',
	},
	{
		id: 'details',
		Header: '',
		width: 100,
		sortable: false,
		filterable: false,
		className: 'actionCell',
		accessor: 'uuid',
		Cell: (d: { value: string }) => (
			<>
				<TETableLink to={`/admin/unit/${d.value}`}>Details</TETableLink>
				{removeMember && (
					<TETableButton onClick={() => removeMember(groupUUID, d.value)}>Unassign</TETableButton>
				)}
				</>
		),
	},
]
