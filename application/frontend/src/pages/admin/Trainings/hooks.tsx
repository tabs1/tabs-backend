import { useState, useEffect, useCallback } from 'react'

import { apiRequest } from 'helpers'
import { useAppContext } from 'hooks'
import { TrainingModel, SortedData, FilteredData } from 'interfaces'

export const useTrainings = (currentPage: number, pageSize: number, sorted: SortedData | null, filtered: FilteredData[],) => {
	const { userToken } = useAppContext()

	const [trainingArray, setTrainingArray] = useState<TrainingModel[]>([])
	const [trainingsLoaded, setTrainingsLoaded] = useState(false)
	const [totalPages, setTotalPages] = useState(1)

	const loadData = useCallback(() => {
		const loadData = async () => {
			try {
				const params: any = {}
                if (filtered) {
                    var filter: { [id: string] : string } = {};
                    for (var i = 0; i < filtered.length; i++) {
                        filter[filtered[i].id] = filtered[i].value
                    }
                    params.filter = filter
                }
				if (sorted) {
					params.order_by = { [sorted.id]: sorted.desc ? 'desc' : 'asc' }
				}
				const response: {
					data: { trainings: TrainingModel[]; Pages: number }
				} = await apiRequest({
					method: 'GET',
					headers: { Authorization: userToken },
					path: `trainings/${currentPage}/${pageSize}`,
					params,
				})

				const { trainings = [], Pages = 1 } = response && response.data
				setTrainingArray(trainings)
				setTrainingsLoaded(true)
				setTotalPages(Pages)
			} catch (e) {
				console.log(e)
				setTrainingArray([])
				setTrainingsLoaded(true)
				setTotalPages(1)
			}
		}
		loadData()
	}, [userToken, currentPage, pageSize, sorted])

	useEffect(() => {
		loadData()
		return () => {
			setTrainingsLoaded(false)
		}
	}, [userToken, currentPage, pageSize, loadData])

	return { trainingArray, trainingsLoaded, totalPages, loadData }
}
