import React from 'react'

import TETableLink from 'components/TETableLink'

import { filterDateColumn, convertToDateShort } from 'helpers'
import TETableButton from 'components/TETableButton'

export const tableColumns = (createCase: (memberUuid: string) => void) => [
	{
		id: 'task_name',
		Header: 'Training Name',
		accessor: 'task_name',
	},
	{
		id: 'task_number',
		Header: 'Training ID',
		accessor: 'task_number',
	},
	{
		id: 'created_at',
		Header: 'Date Created',
		filterMethod: (filter: any, row: any) => filterDateColumn(convertToDateShort, filter, row),
		accessor: 'created_at',
		Cell: (d: any) => convertToDateShort(d.value),
	},
	{
		id: 'frequency',
		Header: 'Frequency',
		accessor: 'frequency',
	},
	{
		id: 'actions',
		Header: '',
		width: 100,
		sortable: false,
		filterable: false,
		className: 'actionCell',
		accessor: 'uuid',
		Cell: (d: any) => (
			<>
				<TETableLink to={`/admin/training/${d.value}`}>Details</TETableLink>
			</>
		),
	},
]
