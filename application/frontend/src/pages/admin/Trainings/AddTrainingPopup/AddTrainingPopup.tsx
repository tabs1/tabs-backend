import React, { useState, ChangeEvent } from 'react'
import { TEPopupForm, TEInputRow, useTEPopups } from 'react-tec'

import TEPopupTitle from 'components/TEPopupTitle'

import { useAppContext } from 'hooks'

import { addTraining } from './requests'
import { TrainingModel, emptyTraining } from 'interfaces'

interface Props {
	visible: boolean
	onSubmit(): void
	onClose(): void
	loadData(): void
}
const AddTrainingPopup = (props: Props) => {
	const { visible, onSubmit, onClose, loadData } = props

	const { userToken } = useAppContext()
	const popupFunctions = useTEPopups()
	const [training, setTraining] = useState<TrainingModel>(emptyTraining)

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
		try {
			const data = {
				training,
				userToken,
				popupFunctions,
			}
			await addTraining(data)
			onSubmit()
			loadData()
		} catch (e) {
			console.log(e)
		}
	}

	return (
		<TEPopupForm visible={visible} onClose={onClose} onSubmit={handleSubmit}>
			<TEPopupTitle>Add Training</TEPopupTitle>
			<TEInputRow
				labelForKey='task_name'
				title='Training Name'
				value={training.task_name}
				onChange={(e: ChangeEvent<HTMLInputElement>) => {
					setTraining({ ...training, task_name: e.target.value })
				}}
				required
			/>
			<TEInputRow
				labelForKey='task_number'
				title='Training ID'
				value={training.task_number}
				onChange={(e: ChangeEvent<HTMLInputElement>) => {
					setTraining({ ...training, task_number: e.target.value })
				}}
				required
			/>
			<TEInputRow
				labelForKey='frequency'
				title='Frequency'
				value={training.frequency}
				onChange={(e: ChangeEvent<HTMLInputElement>) => {
					setTraining({ ...training, frequency: e.target.value })
				}}
				required
			/>
		</TEPopupForm>
	)
}

export default AddTrainingPopup
