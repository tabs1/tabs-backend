import React, { useState } from 'react'
import { TEPanelWrapper, TEPanel, useTEPopups } from 'react-tec'

import TEReactTable from 'components/TEReactTable'
import TEPanelActionButton from 'components/TEPanelActionButton'

import { useBarTitle, useAppContext } from 'hooks'
import { SortedData, FilteredData } from 'interfaces'

import AddTrainingPopup from './AddTrainingPopup'
import UploadTrainingPopup from './UploadTrainingPopup'

import { useTrainings } from './hooks'
import { tableColumns } from './tableColumns'
import TEPanelActionWrapper from 'components/TEPanelActionWrapper'

import { createCase } from './requests'

const defaultSort = {
	id: 'created_at',
	desc: false,
}

const Trainings = () => {
	useBarTitle('Trainings')
	const { userToken } = useAppContext()
	const popupFunctions = useTEPopups()
	const [addTrainingPopupVisible, setAddTrainingPopupVisible] = useState(false)
	const [uploadTrainingPopupVisible, setUploadTrainingPopupVisible] = useState(false)
	const [currentPage, setCurrentPage] = useState(1)
	const [pageSize, setPageSize] = useState(10)
	const [sorted, setSorted] = useState<SortedData | null>(defaultSort)
    const [filtered, setFiltered] = useState<FilteredData[]>([])

	const { trainingArray, trainingsLoaded, totalPages, loadData } = useTrainings(
		currentPage,
		pageSize,
		sorted,
        filtered,
	)

	const handleCreateCase = (target_uid: string) => {
		const data = {
			training_uid: target_uid,
			userToken,
			popupFunctions,
		}
		createCase(data)
		loadData()
	}

	return (
		<>
			<TEPanelWrapper>
				<TEPanel
					title='Trainings'
					rightComponent={
						<TEPanelActionWrapper>
							<TEPanelActionButton onClick={() => setUploadTrainingPopupVisible(true)}>
								Upload Training File
							</TEPanelActionButton>
							<TEPanelActionButton onClick={() => setAddTrainingPopupVisible(true)}>
								Add Training
							</TEPanelActionButton>
						</TEPanelActionWrapper>
					}
				>
					<TEReactTable
						data={trainingArray}
						columns={tableColumns(handleCreateCase)}
						defaultSorted={[defaultSort]}
						loading={!trainingsLoaded}
                        manual
                        filterable
						pages={totalPages}
						onFetchData={(state: {
							page: number
							pageSize: number
							sorted: SortedData[]
                            filtered: FilteredData[]
						}) => {
							setCurrentPage(state.page + 1)
							setPageSize(state.pageSize)
							if (state.sorted.length > 0) {
								setSorted(state.sorted[0])
							} else {
								setSorted(null)
							}
                            setFiltered(state.filtered)
						}}
					/>
				</TEPanel>
			</TEPanelWrapper>
			<AddTrainingPopup
				visible={addTrainingPopupVisible}
				onSubmit={() => setAddTrainingPopupVisible(false)}
				onClose={() => setAddTrainingPopupVisible(false)}
				loadData={loadData}
			/>
			<UploadTrainingPopup
				visible={uploadTrainingPopupVisible}
				onSubmit={() => setUploadTrainingPopupVisible(false)}
				onClose={() => setUploadTrainingPopupVisible(false)}
				loadData={loadData}
			/>
		</>
	)
}

export default Trainings
