import React from 'react'
import { Route, Switch } from 'react-router-dom'

import NotFound from 'pages/NotFound'

import Home from 'pages/front/Home'

import { MainPanel, FrontContentWrapper } from './styledComponents'

const ContentWrapper = () => {
	return (
		<MainPanel>
			<FrontContentWrapper>
				<Switch>
					<Route exact path='/' component={Home} />

					<Route component={NotFound} />
				</Switch>
			</FrontContentWrapper>
		</MainPanel>
	)
}

export default ContentWrapper
