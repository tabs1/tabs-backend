from application import db
from marshmallow import ValidationError, EXCLUDE
from sqlalchemy.exc import IntegrityError
from sqlalchemy import and_
import logging

class BaseClassService():

    def __init__(self, instance=None):

        if not(hasattr(self, 'Model') and hasattr(self, 'Serializer')):
            raise Exception('Service Class needs Model and Serializer defined')

        status_code = 200
        errors = {}
        if type(instance) == str:
            instance = self.Model.query.get(instance)
            if not instance:
                status_code = 404
                errors = { 'resource' : self.Model.__name__.replace('Model', "") + ' not found' }
        elif instance and type(instance) is not self.Model:
            raise Exception('Model instance must match Service Model')

        self.instance = instance
        self.errors = errors
        self.status_code = status_code


    def load(self, instance, status_code=200):
        self.instance = instance
        self.status_code = status_code
        self.errors = {}


    def get(self, attr):
        return getattr(self.instance, attr, None)


    def _crud_error_handler(self, error):
        self.status_code = 417
        logging.error(error)
        if isinstance(error, ValueError):
            self.errors = { 'error' : error }
        elif isinstance(error, ValidationError):
            self.errors = error.messages
        else:
            db.session.rollback()
            message = error.orig.args[0].split("\n", 1)[1].split(" ", 1)[1]
            self.errors = { 'unique_constraint' : message.strip() }


    ## CRUD
    def create(self, data):
        data.update(self.Model.defaults())
        try:
            model_data = self.Serializer(unknown=EXCLUDE).load(data)
            instance = self.Model(**model_data)

            db.session.add(instance)
            db.session.commit()
            self.load(instance, 201)

        except (ValidationError, ValueError, IntegrityError) as error:
            self._crud_error_handler(error)

        return self

    def get_or_create(self, parameters, item):
        """
        Takes a list of parameters and a dictionary
        """
        keys = item.keys()
        for parameter in parameters:
            if not self.instance and parameter in keys:
                null_fields = ["", "none", "unk", "n/a", "no bid", "not found", "nd5", "nds(no id)", "unknown", "no"]
                if item[parameter].lower() in null_fields:
                    item.pop(parameter)
                else:
                    try:
                        object = self.Model.param_query(filter={parameter: item[parameter]}).first()


                        self.instance = object
                        if self.instance:
                            reason = "Found by: " + parameter + " with value " + item[parameter]
                            self.update(item)
                            return self, False, None, reason

                    except Exception as e:
                        logging.error(e)
                        return self, False, e, "Error while searching"

        if not self.instance:
            self.create(item)
            if self.errors:
                return self, False, "Error adding", "Error Adding member: "+ str(self.errors)

        return self, True, None, None


    def update(self, data):
        if self.instance:
            for field in self.Model._restricted_update_fields + ['uuid']:
                try:
                    data.pop(field)
                except:
                    pass
            try:
                validated = self.Serializer(unknown=EXCLUDE, partial=True).load(data)
                for field in data.keys():
                    setattr(self.instance, field, validated[field])
                db.session.add(self.instance)
                db.session.commit()
                self.load(self.instance)
            except (ValidationError, ValueError, IntegrityError) as error:
                logging.error(error)
                self._crud_error_handler(error)

        return self


    def delete(self):
        if self.instance:
            has_associations = False
            for relationship in self.Model.relationships():
                if self.count_associations(relationship):
                    has_associations = True

            if has_associations:
                self.errors['delete'] = 'has existing associations'
                self.status_code = 417
            else:
                db.session.delete(self.instance)
                db.session.commit()
                self.load(None)

        return self


    # Relationships
    def count_associations(self, relationship):
        count = 0
        if self.instance:
            try:
                count = len(getattr(self.instance, relationship))
            except:
                # handle dynamic subquery
                count = getattr(self.instance, relationship).count()

        return count


    def _find_relationship(self, relationship, object):
        associations = getattr(self.Model, relationship) \
                        .any(and_(getattr(type(object), "uuid")==object.uuid, self.Model.uuid==self.instance.uuid))
        return self.Model.query.filter(associations)


    def append(self, relationship, service):
        if self.instance:
            relations = self._find_relationship(relationship, service.instance)

            if relations.count() == 0:
                relation = getattr(self.instance, relationship).append(service.instance)
                db.session.add(self.instance)
                db.session.commit()
                self.load(self.instance)

        return self


    def remove(self, relationship, service):
        if self.instance:
            getattr(self.instance, relationship).remove(service.instance)
            db.session.commit()
            self.load(self.instance)

        return self


    def remove_all(self, relationship):
        if self.instance:
            getattr(self.instance, relationship)[:] = []
            db.session.commit()
            self.load(self.instance)

        return self


    # Serialization
    def serialize(self, excludes=[]):
        all_excludes = excludes + self.Model._restricted_serializer_fields
        return self.Serializer(exclude=all_excludes).dump(self.instance)

    def simple_response(self, omit_nested=True, excludes=[]):
        response = None
        if self.errors:
            response = { 'errors' : self.errors }
        elif self.instance:
            relations = self.Model.relationships() if omit_nested else []
            all_excludes = relations + excludes
            response = self.serialize(excludes=all_excludes)

        return response, self.status_code
