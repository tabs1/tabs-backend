from datetime import datetime

from application import Group, GroupSerializer
from application.services.base_class_service import BaseClassService


class GroupService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = Group
        self.Serializer = GroupSerializer
        super().__init__(instance)
