from datetime import datetime

from application import Unit, UnitSerializer
from application.services.base_class_service import BaseClassService


class UnitService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = Unit
        self.Serializer = UnitSerializer
        super().__init__(instance)
