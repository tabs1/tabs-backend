from datetime import datetime

from application import Training, TrainingSerializer
from application.services.base_class_service import BaseClassService


class TrainingService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = Training
        self.Serializer = TrainingSerializer
        super().__init__(instance)
