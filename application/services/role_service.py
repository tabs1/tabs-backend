from datetime import datetime

from application import Role, RoleSerializer
from application.services.base_class_service import BaseClassService


class RoleService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = Role
        self.Serializer = RoleSerializer
        super().__init__(instance)
