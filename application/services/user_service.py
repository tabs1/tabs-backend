from application.services.base_class_service import BaseClassService
from application import User, UserSerializer
import logging
from application import db


class UserService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = User
        self.Serializer = UserSerializer
        super().__init__(instance)
