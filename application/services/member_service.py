from datetime import datetime

from application import Member, MemberSerializer
from application.services.base_class_service import BaseClassService


class MemberService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = Member
        self.Serializer = MemberSerializer
        super().__init__(instance)
