from application.services.base_class_service import BaseClassService
from application import File, FileSerializer
import logging
from application import db


class FileService(BaseClassService):

    def __init__(self, instance=None):
        self.Model = File
        self.Serializer = FileSerializer
        super().__init__(instance)
