from marshmallow import Schema, fields, validate, post_load, validates_schema, ValidationError

import bcrypt, re

from application import app

# Marshmallow validators
username_validator = validate.Length(min=6, max=70, error='Fails Length Restrictions')
password_validator = validate.Length(min=10, max=70, error='Fails Length Restrictions')

class UserSerializer(Schema):
    roles = fields.Nested('RoleSerializer',many=True, exclude=['users'])
    units = fields.Nested('UnitSerializer',many=True, exclude=['users'])

    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)

    username = fields.Str(required=True, validate=username_validator)
    password = fields.Str(load_only=True, required=True, validate=password_validator)
    is_active = fields.Boolean(required=True)
    group_admin = fields.Boolean(required=True)
    write_privileges = fields.Boolean(required=True)
    email_address = fields.Str(required=False)

    @validates_schema
    def validate_schema(self, data, **kwargs):
        if 'username' in data and not self.username_constraints(data['username']):
            error_message = 'Only alpha numeric with dashes and underscores'
            raise ValidationError(error_message, "username")

        if 'password' in data:
            error_message = 'Does not meet required characters (A-Z a-z 0-9 ' + app.config['ALLOWED_PASSWORD_PUNCTUATION'] + ')'
            if not self.password_requirements(data['password']):
                raise ValidationError(error_message, "password")

            if not self.password_constraints(data['password']):
                raise ValidationError(error_message, "password")


    @post_load
    def postloader(self, data, **kwargs):
        if data.get("password"):
            data["password"] = self.hash_password(data.get("password"))
        return data


    @staticmethod
    def hash_password(password):
        return bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(app.config['BCRYPT_CYCLES'])).decode('utf-8')

    @staticmethod
    def password_requirements(password):
        ## must have at least one match per condition
        return ( bool(re.search('[0-9]', password)) \
            and bool(re.search('[a-z]', password)) \
            and bool(re.search('[A-Z]', password)) \
            and bool(re.search('[' + re.escape(app.config['ALLOWED_PASSWORD_PUNCTUATION']) + ']', password)))


    @staticmethod
    def password_constraints(password):
        ## only as defined
        return bool(re.match('^[a-zA-Z0-9' + re.escape(app.config['ALLOWED_PASSWORD_PUNCTUATION']) + ']*$', password))


    @staticmethod
    def username_constraints(username):
        ## only alpha-numeric with dashes/underscores (as long as not leading)
        return ( bool(re.search('^[a-zA-Z0-9' + re.escape('-_') + ']*$', username)) \
            and not bool(re.search('^[' + re.escape('-_') + ']', username)) )
