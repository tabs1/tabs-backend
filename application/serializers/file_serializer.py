from marshmallow import Schema, fields, validate, pre_load

class FileSerializer(Schema):
    # BaseClass
    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    
    # Fields
    name = fields.Str(required=True)
    poc = fields.Str(required=False)

    name = fields.Str(required=True)
    bucket = fields.Str(required=False)
    date_created = fields.DateTime(required=False)
    type = fields.Str(required=False)
    derog = fields.Boolean(required=True)
    description = fields.Str(required=False)
    classification = fields.Str(required=False)
    dissemination = fields.Str(required=False)

    user = fields.Str(required=False)
    member = fields.Str(required=False)
    case = fields.Str(required=False) 