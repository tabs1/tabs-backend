from marshmallow import Schema, fields, validate, pre_load

class UnitSerializer(Schema):
    # BaseClass
    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)

    #Relationships
    # groups = fields.Nested('GroupSerializer',many=True, exclude=['units'])
    users = fields.Nested('UserSerializer',many=True, exclude=['units'])

    #members = fields.Nested('MemberSerializer', exclude=['uni'])

    # Fields
    name = fields.Str(required=True)
    poc = fields.Str(required=False)

    highest_echelon = fields.Bool(required=True)
    lowest_echelon = fields.Bool(required=True)
