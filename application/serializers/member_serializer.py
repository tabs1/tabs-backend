from marshmallow import Schema, fields, validate, pre_load

class MemberSerializer(Schema):
    # BaseClass
    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)

    groups = fields.Nested('GroupSerializer', many=True, exclude=['members'])
    trainings = fields.Nested('TrainingSerializer', many=True, exclude=['members'])


    # Basic Fields
    last_name = fields.Str(required=False, allow_none=True)
    middle_name = fields.Str(required=False, allow_none=True)
    first_name = fields.Str(required=False, allow_none=True)
    rank_short = fields.Str(required=False, allow_none=True)
    last_four = fields.Str(required=False, allow_none=True)
    pay_grade_code = fields.Str(required=False, allow_none=True)
    mos = fields.Str(required=False, allow_none=True)
    smos =  fields.Str(required=False, allow_none=True)
    date_of_rank = fields.Str(required=False, allow_none=True)
    basd = fields.Str(required=False, allow_none=True)
    ets_date = fields.Str(required=False, allow_none=True)
    pcs_date = fields.Str(required=False, allow_none=True)
    opcon_control = fields.Str(required=False, allow_none=True)
    adcon_control = fields.Str(required=False, allow_none=True)
    
