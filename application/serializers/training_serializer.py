from marshmallow import Schema, fields, validate, pre_load

class TrainingSerializer(Schema):
    # BaseClass
    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)

    # Relationships
    group_id = fields.Str(required=False, allow_none=True)
    # Note on above definition - this is not a formal relationship because
    # formalizing the relationship could potentially result in circular 
    # class dependencies under the current construct. This is a band-aid solution
    # but should function without a noticable performance hit with small datasets.
    members = fields.Nested('MemberSerializer', many=True, exclude=['trainings'])

    # Basic Fields
    task_number = fields.Str(required=False, allow_none=True)
    task_name = fields.Str(required=False, allow_none=True)
    frequency = fields.Str(required=False, allow_none=True)

