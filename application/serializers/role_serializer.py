from marshmallow import Schema, fields, validate, pre_load

class RoleSerializer(Schema):
    # BaseClass
    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    name = fields.Str(required=False, allow_none=True)

    users = fields.Nested('UserSerializer',many=True, exclude=['roles'])
