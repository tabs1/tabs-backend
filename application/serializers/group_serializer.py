from marshmallow import Schema, fields, validate

class GroupSerializer(Schema):
    uuid = fields.Str(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    
    members = fields.Nested('MemberSerializer', many=True, exclude=['groups'])

    # Fields
    name = fields.Str(required=True)
    role = fields.Str(required=True)
