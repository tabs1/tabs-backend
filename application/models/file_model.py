from datetime import datetime
from application import db, app

from application.models.base_class_model import BaseClassModel
from application.models.group_model import Group
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship, backref

class File(db.Model, BaseClassModel):
    __tablename__ = 'files'

    name = db.Column(db.String(63), nullable=False)
    bucket = db.Column(db.String(63), nullable=True)
    date_created = db.Column(db.DateTime, nullable=True)
    type = db.Column(db.String(63), nullable=True)
    derog = db.Column(db.Boolean, nullable=True, default=False)
    description = db.Column(db.String(63), nullable=True)
    classification = db.Column(db.String(63), nullable=True)
    dissemination = db.Column(db.String(63), nullable=True)

    user = db.Column(db.String(63), nullable=True, index=True)
    member = db.Column(db.String(63), nullable=True, index=True)


