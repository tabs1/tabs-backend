from application import db

from application.models.base_class_model import BaseClassModel
from .user_model import User
from sqlalchemy.dialects.postgresql import UUID
from .training_model import Training
from .member_model import Member

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, backref


class MemberTraining (db.Model, BaseClassModel):
    __tablename__ = 'member_training'

    # Relationships
    member = relationship(Member, backref=backref("member_training"), lazy='subquery')
    training = relationship(Training, backref=backref("member_training"), lazy='subquery')

    # Fields
    member_id = db.Column(UUID(as_uuid=True), db.ForeignKey('members.uuid'))
    training_id = db.Column(UUID(as_uuid=True), db.ForeignKey('trainings.uuid'))
