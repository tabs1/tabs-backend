from application import db

from application.models.base_class_model import BaseClassModel
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

# Define the Role data-model
class Role(db.Model, BaseClassModel):
    __tablename__ = 'roles'
    users = relationship("User", secondary="user_role", lazy='subquery')

    name = db.Column(db.String(50), unique=True)