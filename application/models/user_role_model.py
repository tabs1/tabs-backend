from application import db

from application.models.base_class_model import BaseClassModel
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import UUID
from .user_model import User
from .role_model import Role

# Define the UserRoles association table
class UserRole(db.Model, BaseClassModel):
    __tablename__ = 'user_role'

    role = relationship(Role, backref=backref("user_role"), lazy='subquery')
    user = relationship(User, backref=backref("user_role"), lazy='subquery')

    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('users.uuid', ondelete='CASCADE'))
    role_id = db.Column(UUID(as_uuid=True), db.ForeignKey('roles.uuid', ondelete='CASCADE'))