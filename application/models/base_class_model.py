from application import db
import datetime

from sqlalchemy.dialects.postgresql import UUID
from flask_sqlalchemy import sqlalchemy
from sqlalchemy.inspection import inspect

class BaseClassModel(object):

    def __repr__(self):
        return '<' + self.__name__ + ' {}>'.format(self.id)

    def __eq__(self, other):
        return self.uuid == other.uuid


    # Never serialize (ie large associations)
    _restricted_serializer_fields = []
    # Rmove from update payloads (ie UUID)
    _restricted_update_fields = []

    uuid = db.Column(UUID(as_uuid=True), primary_key=True,
                    server_default=sqlalchemy.text("uuid_generate_v4()"))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow,
                    nullable=False)



    @classmethod
    def relationships(cls):
        relations = []
        linking = []
        for relation in inspect(cls).relationships.items():
            relations.append(relation[0])
            secondary = str(relation[1].secondary)
            if secondary:
                linking.append(secondary)

        return [x for x in relations if x not in linking]


    @classmethod
    def param_query(cls, filter={}, order_by={}, pattern={}):
        query = cls.query

        for attr, value in filter.items():
            try:
                query = query.filter( getattr(cls, attr)==value )
            except:
                pass

        for attr, value in pattern.items():
            try:
                query = query.filter( getattr(cls, attr).like(value) )
            except:
                pass

        for attr, direction in order_by.items():
            try:
                query = query.order_by( getattr(getattr(cls, attr), direction.lower())() )
            except:
                pass

        return query


    @staticmethod
    def defaults():
        return {}
