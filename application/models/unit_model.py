from datetime import datetime
from application import db, app

from application.models.base_class_model import BaseClassModel
from application.models.group_model import Group
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship, backref

class Unit(db.Model, BaseClassModel):
    __tablename__ = 'units'

    name = db.Column(db.String(63), nullable=False)
    poc = db.Column(db.String(63), nullable=True)

    lowest_echelon = db.Column(db.Boolean, nullable=False)
    highest_echelon = db.Column(db.Boolean, nullable=False)
    higher_echelon_id = db.Column(db.String(63), nullable=True)

    users = relationship("User", secondary="unit_user", lazy='subquery')
