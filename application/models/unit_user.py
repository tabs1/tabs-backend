from application import db

from application.models.base_class_model import BaseClassModel
from .user_model import User
from sqlalchemy.dialects.postgresql import UUID
from .unit_model import Unit

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, backref


class UnitUser (db.Model, BaseClassModel):
    __tablename__ = 'unit_user'

    # Relationships
    unit = relationship(Unit, backref=backref("unit_user"), lazy='subquery')
    user = relationship(User, backref=backref("unit_user"), lazy='subquery')
    
    # Fields

    # Should allow a user to view information about a unit and its members
    # read_priv = db.Column(db.Boolean, nullable=False)

    # # Should allow a user to edit information about a unit and its members
    # write_priv = db.Column(db.Boolean, nullable=False)

    # # Should allow a user to add other users to administer a unit, set privileges,
    # # add and remove taskings, and other actions deemed more significant than simple editing
    # admin_priv = db.Column(db.Boolean, nullable=False)
    unit_id = db.Column(UUID(as_uuid=True), db.ForeignKey('units.uuid'))
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('users.uuid'))
