from application import db

from application.models.base_class_model import BaseClassModel
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

## hashing
import bcrypt

class User(db.Model, BaseClassModel):
    __tablename__ = 'users'

    # Relationships
    roles = relationship("Role", secondary="user_role", lazy='subquery')
    units = relationship("Unit", secondary="unit_user", lazy='subquery')

    # Fields
    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    group_admin = db.Column(db.Boolean, nullable=False)
    write_privileges = db.Column(db.Boolean, nullable=False)
    is_active = db.Column(db.Boolean, nullable=False)
    email_address = db.Column(db.String(255), nullable=True, unique=True)

    @staticmethod
    def get_and_validate(username, password):
        user = User.query.filter_by(username=username).first()
        if user \
            and user.is_active \
            and bcrypt.checkpw(password.encode('utf8'), user.password.encode('utf8')):
            return user
        else:
            return None


    @staticmethod
    def defaults():
        return { "is_active" : True }
