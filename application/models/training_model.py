from datetime import datetime
from application import db, app

from application.models.base_class_model import BaseClassModel
from application.models.user_model import User
from application.models.unit_model import Unit
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship, backref
from sqlalchemy.schema import CheckConstraint

class Training(db.Model, BaseClassModel):
    __tablename__ = 'trainings'
    __table_args__ = (
        CheckConstraint('coalesce(task_number, task_name ) is not null'),
    )

    # Relationships

    # Denotes a relationship that shows when a member of the unit completed a training
    # Scheduled function will mark trainings older than the allowable frequency as such
    members = relationship("Member", secondary="member_training", lazy='subquery')

    # The group this training is assigned to. Members can be assigned to more than one group
    group_id = db.Column(db.String(127), nullable=True)
    # Note on above definition - this is not a formal relationship because
    # formalizing the relationship could potentially result in circular 
    # class dependencies under the current construct. This is a band-aid solution
    # but should function without a noticable performance hit with small datasets.

    # Basic Fields

    # String that uniquely identifies a particular training
    task_number = db.Column(db.String(127), nullable=True)

    # Title or description of a particular training
    task_name = db.Column(db.String(127), nullable=True)

    # Training frequency requirement in days
    frequency = db.Column(db.String(127), nullable=True)


    @staticmethod
    def defaults():
        return {
            # Place default values here, could potentially make this a configuration
            # piece if needed
        }
