from application import db
from application.models.base_class_model import BaseClassModel
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import relationship, backref

class Group(db.Model, BaseClassModel):
    __tablename__ = 'groups'

    # Relationships
    members = relationship("Member", secondary="member_group", lazy='subquery')

    # Fields
    name = db.Column(db.String(63), nullable=True)
    role = db.Column(db.String(63), nullable=True)
