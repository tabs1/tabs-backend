from application import db

from application.models.base_class_model import BaseClassModel
from .member_model import Member
from .group_model import Group

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, backref


class MemberGroup(db.Model, BaseClassModel):
    __tablename__ = 'member_group'

    # Relationships
    member = relationship(Member, backref=backref("member_group"), lazy='subquery')
    group = relationship(Group, backref=backref("member_group"), lazy='subquery')

    # Fields
    member_id = db.Column(UUID(as_uuid=True), db.ForeignKey('members.uuid'))
    group_id = db.Column(UUID(as_uuid=True), db.ForeignKey('groups.uuid'))
