from datetime import datetime
from application import db, app

from application.models.base_class_model import BaseClassModel
from application.models.user_model import User
from application.models.unit_model import Unit
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy.orm import relationship, backref
from sqlalchemy.schema import CheckConstraint

class Member(db.Model, BaseClassModel):
    __tablename__ = 'members'
    __table_args__ = (
        CheckConstraint('coalesce( last_four, last_name ) is not null'),
    )

    # Relationships
    groups = relationship("Group", secondary="member_group", lazy='subquery')
    trainings = relationship("Training", secondary="member_training", lazy='subquery')
    
    unit = db.Column(db.String(127), nullable=True)
    
    # Basic Fields
    last_name = db.Column(db.String(127), nullable=True)
    middle_name = db.Column(db.String(127), nullable=True)
    first_name = db.Column(db.String(127), nullable=True)
    rank_short = db.Column(db.String(127), nullable=True)
    last_four = db.Column(db.String(127), nullable=True)
    pay_grade_code = db.Column(db.String(127), nullable=True)
    mos = db.Column(db.String(127), nullable=True)
    smos =  db.Column(db.String(127), nullable=True)
    date_of_rank = db.Column(db.String(127), nullable=True)
    basd = db.Column(db.String(127), nullable=True)
    ets_date = db.Column(db.String(127), nullable=True)
    pcs_date = db.Column(db.String(127), nullable=True)
    opcon_control = db.Column(db.String(127), nullable=True)
    adcon_control = db.Column(db.String(127), nullable=True)


    @staticmethod
    def defaults():
        return {
            # Place default values here, could potentially make this a configuration
            # piece if needed
        }
