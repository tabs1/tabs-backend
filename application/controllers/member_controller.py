import requests
import json

from flask import jsonify, request, g, session, redirect
from flask_user import roles_required
import logging
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller


@app.route('/member/<uuid>', methods=['GET'])
@Auth.required
def member_get(uuid):
    member_service = MemberService(uuid)
    packet, code = member_service.simple_response(omit_nested=False)
    return jsonify(packet), code

# def google_search(search):
#     d = {column: search for column in columns}
#     raw = [
#         model.query.filter(getattr(model, col).ilike(f"{val}%")).all()
#         for col, val in d.items()
#     ]
#     return [item for item in raw if item]

@app.route("/member", methods=["POST"])
@Auth.required
def member_post():
    payload = request.get_json()
    member_service = MemberService().create(payload)
    packet, code = member_service.simple_response()
    return jsonify(packet), code


@app.route('/member/<uuid>', methods=['POST'])
@Auth.required
def member_uuid_post(uuid):
    payload = request.get_json()
    member_service = MemberService(uuid).update(payload)
    packet, code = member_service.simple_response()
    return jsonify(packet), code

@app.route('/member/advance/<uuid>', methods=['POST'])
@Auth.required
def member_advance_uuid_post(uuid):
    member_service = MemberService(uuid).update({"claimed" : False,
                    "working_user_id" : None})
    packet, code = member_service.simple_response()
    return jsonify(packet), code

@app.route('/assign_member/current_user/<uuid>', methods=['POST'])
def member_assign_user_post(uuid):
    member = Member.query.get(uuid)
    # user = User.query.get(session["user_id"])
    # user_service = UserService(user)
    # logging.debug(user_service.simple_response(omit_nested=False))
    member_service =  MemberService(member)
    member_service.update({"working_user_id" : str(session["user_id"]),
                           "claimed": True})
    packet, code = member_service.simple_response(omit_nested=False)
    # logging.debug(member_service.simple_response(omit_nested=False))
    # try:
    #     user_service.append("working_members", member_service)
    #     packet, code = user_service.simple_response(omit_nested=False)
    # except Exception as e:
    #     packet, code = user_service.simple_response(omit_nested=False)
    #     logging.error(str(e) + "\n" + str(packet))

    return jsonify(packet), code

@Controller.paginate_route('/members')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def members_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']
    g.query_params["filter"].update(claimed = False)
    members = Member.param_query(**g.query_params).paginate(page, per_page, error_out=False)
    response = Controller.paginated_payload(page, per_page, Member)
    response['members'] = MemberSerializer(many=True).dump(members.items)

    return jsonify(response), 200

@Controller.paginate_route('/mymembers')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def my_members_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']
    g.query_params["filter"].update(working_user_id = str(session["user_id"]))
    members = Member.param_query(**g.query_params).paginate(page, per_page, error_out=False)
    response = Controller.paginated_payload(page, per_page, Member)
    response['members'] = MemberSerializer(many=True).dump(members.items)
    return jsonify(response), 200

@app.route('/members_no_pages', methods=['GET'])
@Auth.required
def members_get_no_pages():
    members = Member.query.all()
    response = {}
    if (members):
        response["members"] = MemberSerializer(many=True).dump(members)
    return jsonify(response), 200
