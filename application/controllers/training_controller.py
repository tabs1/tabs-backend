import requests
import json

from flask import jsonify, request, g, session, redirect
from flask_user import roles_required
import logging
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller


@app.route('/training/<uuid>', methods=['GET'])
@Auth.required
def training_get(uuid):
    training_service = TrainingService(uuid)
    packet, code = training_service.simple_response(omit_nested=False)
    return jsonify(packet), code

# def google_search(search):
#     d = {column: search for column in columns}
#     raw = [
#         model.query.filter(getattr(model, col).ilike(f"{val}%")).all()
#         for col, val in d.items()
#     ]
#     return [item for item in raw if item]

@app.route("/training", methods=["POST"])
@Auth.required
def training_post():
    payload = request.get_json()
    logging.debug(payload)
    training_service = TrainingService().create(payload)
    packet, code = training_service.simple_response()
    return jsonify(packet), code


@app.route('/training/<uuid>', methods=['POST'])
@Auth.required
def training_uuid_post(uuid):
    payload = request.get_json()
    training_service = TrainingService(uuid).update(payload)
    packet, code = training_service.simple_response()
    return jsonify(packet), code

@app.route('/training/advance/<uuid>', methods=['POST'])
@Auth.required
def training_advance_uuid_post(uuid):
    training_service = TrainingService(uuid).update({"claimed" : False,
                    "working_user_id" : None})
    packet, code = training_service.simple_response()
    return jsonify(packet), code

@app.route('/assign_training/current_user/<uuid>', methods=['POST'])
def training_assign_user_post(uuid):
    training = Training.query.get(uuid)
    # user = User.query.get(session["user_id"])
    # user_service = UserService(user)
    # logging.debug(user_service.simple_response(omit_nested=False))
    training_service =  TrainingService(training)
    training_service.update({"working_user_id" : str(session["user_id"]),
                           "claimed": True})
    packet, code = training_service.simple_response(omit_nested=False)
    # logging.debug(training_service.simple_response(omit_nested=False))
    # try:
    #     user_service.append("working_trainings", training_service)
    #     packet, code = user_service.simple_response(omit_nested=False)
    # except Exception as e:
    #     packet, code = user_service.simple_response(omit_nested=False)
    #     logging.error(str(e) + "\n" + str(packet))

    return jsonify(packet), code

@Controller.paginate_route('/trainings')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def trainings_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']
    g.query_params["filter"].update(claimed = False)
    trainings = Training.param_query(**g.query_params).paginate(page, per_page, error_out=False)
    response = Controller.paginated_payload(page, per_page, Training)
    response['trainings'] = TrainingSerializer(many=True).dump(trainings.items)

    return jsonify(response), 200

@Controller.paginate_route('/mytrainings')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def my_trainings_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']
    g.query_params["filter"].update(working_user_id = str(session["user_id"]))
    trainings = Training.param_query(**g.query_params).paginate(page, per_page, error_out=False)
    response = Controller.paginated_payload(page, per_page, Training)
    response['trainings'] = TrainingSerializer(many=True).dump(trainings.items)
    return jsonify(response), 200

@app.route('/trainings_no_pages', methods=['GET'])
@Auth.required
def trainings_get_no_pages():
    trainings = Training.query.all()
    response = {}
    if (trainings):
        response["trainings"] = TrainingSerializer(many=True).dump(trainings)
    return jsonify(response), 200
