import json

from flask import jsonify, request, g, session
from flask_user import roles_required
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller


@app.route('/user/<uuid>', methods=['GET'])
@Auth.required
def user_get(uuid):
    user_service = UserService(uuid)
    packet, code = user_service.simple_response(omit_nested=False)
    return jsonify(packet), code


@app.route("/user", methods=["POST"])
@Auth.required
def user_post():
    payload = request.get_json()
    relationships = payload.pop("relationships")
    user_service = UserService().create(payload)
    Controller.update_relationships(user_service, relationships)
    packet, code = user_service.simple_response(omit_nested=False)
    return jsonify(packet), code


@app.route('/user/deactivate/<uuid>', methods=['POST'])
@Auth.required
def deactivate_user_post(uuid):
    payload = request.get_json()
    user_service = UserService(uuid)
    user_service.update({"is_active" : False})
    packet, code = user_service.simple_response()
    return jsonify(packet), code

@app.route('/user/<uuid>', methods=['POST'])
@Auth.required
def user_uuid_post(uuid):
    
    payload = request.get_json()
    logging.debug(payload)
    user_service = UserService(uuid)
    relationships = payload.pop("relationships")
    user_service.update(payload)
    Controller.update_relationships(user_service, relationships)

    packet, code = user_service.simple_response()
    return jsonify(packet), code


@Controller.paginate_route('/users')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def users_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']

    users = User.param_query(**g.query_params).paginate(page, per_page, error_out=False)

    response = Controller.paginated_payload(page, per_page, User)
    response['users'] = UserSerializer(many=True).dump(users.items)

    return jsonify(response), 200

@app.route('/users_no_pages', methods=['GET'])
@Auth.required
def users_get_no_pages():
    users = User.query.all()
    response = {}
    if (users):
        response["users"] = UserSerializer(many=True).dump(users)
    return jsonify(response), 200


@app.route('/user/token', methods=['POST'])
def user_token_post():
    data = request.get_json()
    user = User.get_and_validate(data["username"], data["password"])
    if user:
        session["user_id"] = user.uuid
        return jsonify(token=Auth.generate_token(user)), 200

    return jsonify(error=True), 403


@app.route('/user/session', methods=['POST'])
def user_session_post():
    data = request.get_json()
    token_data = Auth.verify_token(data["token"])

    if token_data and ('id' in token_data):
        user_service = UserService(token_data['id'])
        packet, code = user_service.simple_response()
        return jsonify(packet), code

    return jsonify(valid=False), 403

@app.route('/current_user/group/assign', methods=['DELETE'])
@Auth.required
def device_assign_delete():
    payload = request.get_json()
    user_service = UserService(payload["user_uuid"])
    group_service = GroupService(payload["group_uuid"])

    try:
        if "group_uuid" in payload:
            user_service.remove("groups", GroupService(payload["group_uuid"]))
        else:
            user_service.remove_all("groups")

        packet, code = user_service.simple_response(omit_nested=False)

    except Exception as e:
        packet = {"error" : str(e)}
        logging.error("Error removing group:" + str(e))
        code = 503
    return jsonify(packet), code
