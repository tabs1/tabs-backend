import json

from flask import jsonify, request, g
from flask_user import roles_required
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller


@app.route('/group/<uuid>', methods=['GET'])
@Auth.required
def group_get(uuid):
    group_service = GroupService(uuid)
    packet, code = group_service.simple_response(omit_nested=False)
    return jsonify(packet), code

@app.route("/group", methods=["POST"])
@Auth.required
def group_post():
    payload = request.get_json()
    group_service = GroupService().create(payload)
    packet, code = group_service.simple_response()
    return jsonify(packet), code

@app.route('/group/<uuid>', methods=['POST'])
@Auth.required
def group_uuid_post(uuid):
    payload = request.get_json()
    logging.debug(payload)
    group_service = GroupService(uuid)
    relationships = payload.pop("relationships")
    group_service.update(payload)
    Controller.update_relationships(group_service, relationships)
    packet, code = group_service.simple_response()
    return jsonify(packet), code

@app.route('/group/unit/assign', methods=['DELETE'])
@Auth.required
def group_unit_delete():
    payload = request.get_json()
    unit_service = UnitService(payload["unit_uuid"])
    if "group_uuid" in payload.keys():
        group_service = GroupService(payload["group_uuid"])

    try:
        if "group_uuid" in payload:
            unit_service.remove("groups", GroupService(payload["group_uuid"]))
        else:
            unit_service.remove_all("groups")

        packet, code = unit_service.simple_response(omit_nested=False)

    except Exception as e:
        packet = {"error" : str(e)}
        logging.error("Error removing group:" + str(e))
        code = 503
    return jsonify(packet), code

@app.route('/group/user/assign', methods=['DELETE'])
@Auth.required
def group_user_delete():
    payload = request.get_json()
    user_service = UserService(payload["user_uuid"])
    if "group_uuid" in payload.keys():
        group_service = GroupService(payload["group_uuid"])

    try:
        if "group_uuid" in payload:
            user_service.remove("groups", GroupService(payload["group_uuid"]))
        else:
            user_service.remove_all("groups")

        packet, code = user_service.simple_response(omit_nested=False)

    except Exception as e:
        packet = {"error" : str(e)}
        logging.error("Error removing group:" + str(e))
        code = 503
    return jsonify(packet), code

@app.route('/groups_no_pages', methods=['GET'])
@Auth.required
def groups_get_no_pages():
    groups = Group.query.all()
    response = {}
    if (groups):
        response["groups"] = GroupSerializer(many=True).dump(groups)
    return jsonify(response), 200

@Controller.paginate_route('/groups')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def groups_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']

    groups = Group.param_query(**g.query_params).paginate(page, per_page, error_out=False)

    response = Controller.paginated_payload(page, per_page, Group)
    response['groups'] = GroupSerializer(many=True).dump(groups.items)

    return jsonify(response), 200
