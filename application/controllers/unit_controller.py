import json

from flask import jsonify, request, g
from flask_user import roles_required
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller


@app.route('/unit/<uuid>', methods=['GET'])
@Auth.required

def unit_get(uuid):
    unit_service = UnitService(uuid)
    packet, code = unit_service.simple_response(omit_nested=False)
    return jsonify(packet), code                

@app.route("/unit", methods=["POST"])
@Auth.required
def unit_post():
    payload = request.get_json()
    unit_service = UnitService().create(payload)
    packet, code = unit_service.simple_response()
    return jsonify(packet), code


@app.route('/unit/<uuid>', methods=['POST'])
@Auth.required
def unit_uuid_post(uuid):
    payload = request.get_json()
    unit_service = UnitService(uuid).update(payload)
    packet, code = unit_service.simple_response()
    return jsonify(packet), code


@Controller.paginate_route('/units')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def units_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']

    units = Unit.param_query(**g.query_params).paginate(page, per_page, error_out=False)

    response = Controller.paginated_payload(page, per_page, Unit)
    response['units'] = UnitSerializer(many=True).dump(units.items)

    return jsonify(response), 200

@app.route('/units_no_pages', methods=['GET'])
@Auth.required
def units_get_no_pages():
    units = Unit.query.all()
    response = {}
    if (units):
        response["units"] = UnitSerializer(many=True).dump(units)
    return jsonify(response), 200
