import os
from application import app

from flask_user import roles_required
from flask import send_from_directory
import logging

@roles_required('Admin')
@app.route('/')
@app.route('/admin')
@app.route('/admin/<path:path>')
def serve_admin(path=""):
    logging.info("Routed to UI:" + str(path))
    logging.info(os.path.exists(app.static_folder + path))
    if path != "" and os.path.exists(app.static_folder + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')

@app.route('/front')
@app.route('/front/<path:path>')
def serve_front(path=""):
    logging.info("Routed to UI:" + str(path))
    logging.info(os.path.exists(app.static_folder + path))
    if path != "" and os.path.exists(app.static_folder + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')