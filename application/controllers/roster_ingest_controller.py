import requests
import json
import csv
from flask import jsonify, request, g
from flask_user import roles_required
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller

@app.route('/upload', methods=['POST'])
def upload_file():
    null_fields = ["", "none", "unk", "n/a"]
    payload = request.get_json()
    rejected_entries = []
    reasons = []
    for item in payload["rows"]:
        try:
            member_parameters = ["ana_id", "taskera", "bid"]
            if "" in item.keys():
                item.pop("")
            member_service, member_added, error, reason = MemberService().get_or_create(member_parameters, item)
            unit_parameters = ["name"]
            if item["unit"].lower() in null_fields:
                item["unit"] = "Unassigned"
            unit_service, unit_added, error, unit_reason = UnitService().get_or_create(unit_parameters, {"name": item["unit"]})
            member_service.update({"assigned_unit_id" : str(unit_service.instance.uuid)})
            if not member_added:
                item["reason"] = reason
                rejected_entries.append(item)

        except Exception as e:
            db.session.rollback()
            logging.error(e)
            if member_service:
                logging.error(member_service.simple_response())
            item["reason"] = str(e)
            rejected_entries.append(item)
    response = {}
    response["members"] = rejected_entries
    response["success"] = str(len(payload["rows"])-len(rejected_entries)) + \
                    " members uploaded of " + str(len(payload["rows"])) + " submitted"

    return jsonify(response), 200