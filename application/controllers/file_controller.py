import requests
import json

from flask import jsonify, request, g, session, redirect
from flask_user import roles_required
import logging
from application import *
from application.helpers.auth_helper import Auth
from application.helpers.controller_helper import Controller


@app.route('/file/<uuid>', methods=['GET'])
@Auth.required
def file_get(uuid):
    file_service = FileService(uuid)
    packet, code = file_service.simple_response(omit_nested=False)
    return jsonify(packet), code

@app.route("/file", methods=["POST"])
@Auth.required
def file_post():
    payload = request.get_json()
    file_service = FileService().create(payload)
    packet, code = file_service.simple_response()
    return jsonify(packet), code


@app.route('/file/<uuid>', methods=['POST'])
@Auth.required
def file_uuid_post(uuid):
    payload = request.get_json()
    file_service = FileService(uuid).update(payload)
    packet, code = file_service.simple_response()
    return jsonify(packet), code

@app.route('/file/advance/<uuid>', methods=['POST'])
@Auth.required
def file_advance_uuid_post(uuid):
    file_service = FileService(uuid).update({"claimed" : False,
                    "working_user_id" : None})
    packet, code = file_service.simple_response()
    return jsonify(packet), code

@app.route('/assign_file/current_user/<uuid>', methods=['POST'])
def file_assign_user_post(uuid):
    file = File.query.get(uuid)
    # user = User.query.get(session["user_id"])
    # user_service = UserService(user)
    # logging.debug(user_service.simple_response(omit_nested=False))
    file_service =  FileService(file)
    file_service.update({"working_user_id" : str(session["user_id"]),
                           "claimed": True})
    packet, code = file_service.simple_response(omit_nested=False)
    # logging.debug(file_service.simple_response(omit_nested=False))
    # try:
    #     user_service.append("working_files", file_service)
    #     packet, code = user_service.simple_response(omit_nested=False)
    # except Exception as e:
    #     packet, code = user_service.simple_response(omit_nested=False)
    #     logging.error(str(e) + "\n" + str(packet))

    return jsonify(packet), code

@Controller.paginate_route('/files')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def files_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']
    g.query_params["filter"].update(claimed = False)
    files = File.param_query(**g.query_params).paginate(page, per_page, error_out=False)
    response = Controller.paginated_payload(page, per_page, File)
    response['files'] = FileSerializer(many=True).dump(files.items)

    return jsonify(response), 200

@Controller.paginate_route('/myfiles')
@Controller.request_parser(order_by={'created_at':'asc'})
@Auth.required
def my_files_get(page=1, per_page=app.config['RESULTS_PER_PAGE']):
    per_page = per_page if per_page <= app.config['MAX_RESULTS_PER_PAGE'] else app.config['MAX_RESULTS_PER_PAGE']
    g.query_params["filter"].update(working_user_id = str(session["user_id"]))
    files = File.param_query(**g.query_params).paginate(page, per_page, error_out=False)
    response = Controller.paginated_payload(page, per_page, File)
    response['files'] = FileSerializer(many=True).dump(files.items)
    return jsonify(response), 200

@app.route('/files_no_pages', methods=['GET'])
@Auth.required
def files_get_no_pages():
    files = File.query.all()
    response = {}
    if (files):
        response["files"] = FileSerializer(many=True).dump(files)
    return jsonify(response), 200
