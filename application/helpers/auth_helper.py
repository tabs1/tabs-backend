from functools import wraps
import hashlib

from application import app
from application.models.user_model import User

from flask import request, jsonify, g

from itsdangerous import TimedJSONWebSignatureSerializer as JWT
from itsdangerous import SignatureExpired, BadSignature


class Auth():

    @staticmethod
    def generate_token(user, expiration=app.config['TOKEN_EXPIRATION']):
        jwt = JWT(app.config['SECRET_KEY'], expires_in=expiration)
        token = jwt.dumps({
            'id': str(user.uuid),
            'username': user.username,
        }).decode('utf-8')
        return token


    @staticmethod
    def verify_token(token):
        jwt = JWT(app.config['SECRET_KEY'])
        try:
            data = jwt.loads(token)
        except (BadSignature, SignatureExpired):
            return None
        return data

    def required(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if app.config['AUTH_ENABLED']:
                token = request.headers.get('Authorization', None)
                if token:
                    string_token = token.encode('ascii', 'ignore')
                    data = Auth.verify_token(string_token)
                    if data:
                        current_user = User.query.get(data['id'])
                        if current_user.is_active:
                            g.current_user = current_user
                            return f(*args, **kwargs)

                return jsonify(message="Authentication is required to access this resource"), 401
            else:
                return f(*args, **kwargs)

        return decorated
