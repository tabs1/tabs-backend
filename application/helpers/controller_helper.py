from functools import wraps
import math
import json

from flask import jsonify, request, make_response, g
from application import *
from application import app


class Controller():

    @staticmethod
    def paginated_payload(page, per_page, Model):
        count = Model.query.count()

        payload = {
            'Resource Count' : count,
            'Pages' : math.ceil(count/per_page),
            'Current Page' : page,
            'Resources Per Page' : per_page,
        }

        return payload


    def request_parser(**defaults):
        def decorator(f):
            @wraps(f)
            def decorated(*args, **kwargs):
                g.query_params = {}
                try:
                    g.query_params['filter'] = json.loads(request.args.get('filter', '{}'))
                except:
                    g.query_params['filter'] = {}


                try:
                    g.query_params['pattern'] = json.loads(request.args.get('pattern', '{}'))
                except:
                    g.query_params['pattern'] = {}


                order_by = json.dumps(defaults['order_by']) if 'order_by' in defaults else '{}'

                try:
                    g.query_params['order_by'] = json.loads(request.args.get('order_by', order_by))
                except:
                    g.query_params['order_by'] = json.loads(order_by)

                return f(*args, **kwargs)
            return decorated
        return decorator

    def update_relationships(update_service, relationships):
        for field in relationships.keys():
            update_service.remove_all(field)
            service_lookup = {
                "groups" : GroupService,
                "users" : UserService,
                "units" : UnitService,
                "working_members" : MemberService,
                "members" : MemberService
            }
            for item in relationships[field]:
                item_service = service_lookup[field](item["uuid"])
                update_service.append(field, item_service)


    def paginate_route(rule):
        def decorator(f):
            app.add_url_rule(rule, None, f, methods=["GET"])
            app.add_url_rule(rule + "/<int:page>", None, f, methods=["GET"])
            app.add_url_rule(rule + "/<int:page>/<int:per_page>", None, f, methods=["GET"])
            return f
        return decorator
