import os, string, re


#password_punctuation_allowed = re.sub('["\'\\"]', '', string.punctuation)
password_punctuation_allowed = "@#$%^&*-+=_!"
token_expiration_time_in_seconds = 24*60*60     #time in seconds at which tokens expire.

class Config(object):
    ## App
    ENV = os.environ.get('ENVIRONMENT') or 'development'

    # Pagination
    RESULTS_PER_PAGE = 10
    MAX_RESULTS_PER_PAGE = 500

    ## Database
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        "postgresql://postgres:testpass@localhost/tabs"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ## Password
    ALLOWED_PASSWORD_CHARACTERS = string.ascii_letters + string.digits + password_punctuation_allowed
    ALLOWED_PASSWORD_PUNCTUATION = password_punctuation_allowed
    BCRYPT_CYCLES = 12 ### Dont change this after deploy!

    ## Auth
    TOKEN_EXPIRATION = token_expiration_time_in_seconds
    AUTH_ENABLED = True
    SECRET_KEY = os.environ.get('APP_KEY') or 'secret_key'

    ## Files
    FILE_FOLDER = os.environ.get('FILE_FOLDER') or "helpers/"
