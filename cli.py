import click
from application import app
from seeds.main import dbseed

@app.cli.command("dbseed")
def start_seeding():
    dbseed()
