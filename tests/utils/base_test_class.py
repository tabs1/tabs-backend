import unittest
import random
import factory.random

from application import app, db
from seeds.main import dbseed

class TestBase(unittest.TestCase):

    _configs = {}

    def setUp(self):
        self.setConfig()
        db.create_all()
        dbseed()
        self.client = app.test_client()

        seed = random.randint(1, 9999999999999999999)
        factory.random.reseed_random(seed)
        print("Random Seed for Test: " + str(seed))


    def tearDown(self):
        db.session.remove()
        db.drop_all()
        pass


    def setConfig(self):
        app.config['TESTING'] = True
        app.config['ENV'] = 'testing'
        app.config['AUTH_ENABLED'] = True
        app.config['WTF_CSRF_ENABLED'] = False

        # Pagination
        app.config['RESULTS_PER_PAGE'] = 10
        app.config['MAX_RESULTS_PER_PAGE'] = 100

        # Database
        app.config['SQLALCHEMY_DATABASE_URI'] = \
            "postgresql://localhost/tabs_testing"

        for key, value in self._configs:
            app.config[key] = value
