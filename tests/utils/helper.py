# add parent folder to python path
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

# python standard libraries
import copy
import json
import hashlib
import random
import requests
import datetime
import uuid

# test libraries
import unittest
from unittest.mock import MagicMock, patch
import factory
import factory.fuzzy
import pytest

# libraries
from flask import jsonify, g, request
import marshmallow
from marshmallow import exceptions, EXCLUDE, validate
from sqlalchemy import and_
from sqlalchemy.orm import aliased, load_only, contains_eager, selectinload


# our code to test
from application import *
from application.helpers import *

# helpers
from tests.factories import *
from tests.utils import *


if __name__ == '__main__':
    unittest.main()

##
# pretend private functions

def _basic_cast(value):
    if isinstance(value, datetime.datetime):
        return value.__str__()

##
# helper functions

def make_fake_uuid():
    return str(uuid.UUID(int=random.Random().getrandbits(128)))

def make_user_registration(overrides={}):
    data = factory.build(dict, FACTORY_CLASS=UserRegistrationFactory)
    data.update(overrides)
    return data

def make_unit_registration(overrides={}):
    data = factory.build(dict, FACTORY_CLASS=UnitRegistrationFactory)
    return data

def make_geofence_create():
    return factory.build(dict, FACTORY_CLASS=GeofenceCreateFactory)


def cast_dict(data, default=_basic_cast):
    return json.loads(json.dumps(data, default = default))


def make_date_from_now(**kwargs):
    return datetime.datetime.utcnow() + datetime.timedelta(**kwargs)
