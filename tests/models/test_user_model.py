from tests.utils.helper import *


class TestDeviceModel(TestBase):

    ##
    # get_and_validate()

    def test_get_and_validate(self):
        password = '1234'
        hash = UserSerializer.hash_password(password)
        user = UserFactory(password=hash, is_active=True)

        found_user = User.get_and_validate(user.username, password)
        self.assertEqual(user.uuid, str(found_user.uuid))


    def test_get_and_validate_wrong_password(self):
        password = '1234'
        hash = UserSerializer.hash_password(password)
        user = UserFactory(password=hash, is_active=True)

        found_user = User.get_and_validate(user.username, "12345")
        self.assertFalse(found_user)


    def test_get_and_validate_ensure_is_active(self):
        password = '1234'
        hash = UserSerializer.hash_password(password)
        user = UserFactory(password=hash, is_active=False)

        found_user = User.get_and_validate(user.username, "1234")
        self.assertFalse(found_user)

