from tests.utils.helper import *


class TestRelationships(TestBase):

    def test_returns_relationships(self):
        ## This test will fail if the device model changes/adds relationships
        self.assertListEqual(User.relationships(), ['groups', 'working_members'])


class TestParamQuery(TestBase):

    def test_filters(self):
        user_1 = UserFactory(username='DarthSiddeous', is_active=True)
        user_2 = UserFactory(is_active=False)

        users = User.param_query(filter={'username': 'DarthSiddeous'}).all()
        self.assertEqual(len(users), 1)
        self.assertEqual(str(users[0].uuid), user_1.uuid)

        users = User.param_query(filter={'is_active': False}).all()
        self.assertEqual(len(users), 1)
        self.assertEqual(str(users[0].uuid), user_2.uuid)


    def test_pattern_matches(self):
        user_0 = UserFactory(username='Frank', is_active=True)
        user_1 = UserFactory(username='DarthSiddeous', is_active=True)
        user_2 = UserFactory(username='SenorDarth', is_active=False)
        user_3 = UserFactory(username='Ted', is_active=False)

        users = User.param_query(pattern={'username' : 'Darth%'}).all()
        self.assertEqual(len(users), 1)
        self.assertEqual(str(users[0].uuid), user_1.uuid)

        users = User.param_query(pattern={'username' : '%Darth%'}).all()
        self.assertEqual(len(users), 2)
        self.assertEqual(str(users[0].uuid), user_1.uuid)
        self.assertEqual(str(users[1].uuid), user_2.uuid)


    def test_pattern_matches_with_filtering(self):
        user_1 = UserFactory(username='DarthSiddeous', is_active=True)
        user_2 = UserFactory(username='SenorDarth', is_active=False)
        user_3 = UserFactory(username='Ted', is_active=False)

        users = User.param_query(pattern={'username' : '%Darth%'}, filter={"is_active" : False}).all()
        self.assertEqual(len(users), 1)
        self.assertEqual(str(users[0].uuid), user_2.uuid)


    def test_ordering(self):
        user_1 = UserFactory(username='DarthSiddeous')
        user_2 = UserFactory(username='JediDJJazzyjeff')
        user_3 = UserFactory(username='AnakinSkywalker')

        users = User.param_query(order_by={'username': 'desc'}).all()
        self.assertEqual(len(users), 3)
        self.assertEqual(str(users[0].username), user_2.username)

        users = User.param_query(order_by={'username': 'asc'}).all()
        self.assertEqual(len(users), 3)
        self.assertEqual(str(users[0].username), user_3.username)
