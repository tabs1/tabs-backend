from tests.utils.helper import *


class TestCase(TestBase):

    def setUpClass():
        @app.route("/request_parser", methods=["GET"])
        @Controller.request_parser(order_by={"A":"asc"})
        def request_parser():
            return "Hello World"


    ##
    # paginate_headers()

    def test_paginate_headers(self):
        members = MemberFactory.create_batch(2, created_at=datetime.datetime.utcnow())
        serialized = MemberSerializer(many=True).dump(members)

        payload = Controller.paginated_payload(2, 1, Member)
        compare = {
            'Resource Count' : 2,
            'Pages' : 2,
            'Current Page' : 2,
            'Resources Per Page' : 1,
        }
        self.assertDictEqual(payload, compare)



    ##
    # request_parser()

    def test_request_parser__defaults(self):
        with app.app_context():
            response = self.client.open(path='/request_parser', method='GET')
            self.assertDictEqual(g.query_params['order_by'], {"A":"asc"})
            self.assertDictEqual(g.query_params['filter'], {})


    def test_request_parser__sort(self):
        with app.app_context():
            response = self.client.open(path='/request_parser?order_by=' + json.dumps({'B':"desc"}) , method='GET')
            self.assertDictEqual(g.query_params['order_by'], {'B':"desc"})


    def test_request_parser__bad_sort(self):
        with app.app_context():
            response = self.client.open(path='/request_parser?order_by=X', method='GET')
            self.assertDictEqual(g.query_params['order_by'], {"A":"asc"})


    def test_request_parser__filter(self):
        with app.app_context():
            response = self.client.open(path='/request_parser?filter=' + json.dumps({'filter':"me"}), method='GET')
            self.assertDictEqual(g.query_params['filter'], {'filter':"me"})


    def test_request_parser__bad_filter(self):
        with app.app_context():
            response = self.client.open(path='/request_parser?filter=X', method='GET')
            self.assertDictEqual(g.query_params['filter'], {})


    def test_request_parser__pattern(self):
        with app.app_context():
            response = self.client.open(path='/request_parser?pattern=' + json.dumps({'pattern':"%me%"}), method='GET')
            self.assertDictEqual(g.query_params['pattern'], {'pattern':"%me%"})


    def test_request_parser__bad_pattern(self):
        with app.app_context():
            response = self.client.open(path='/request_parser?pattern=X', method='GET')
            self.assertDictEqual(g.query_params['pattern'], {})


    ##
    # paginate_request()

    def test_paginate_request(self):
        @Controller.paginate_route("/paginateroute")
        def paginate_route(page=1, per_page=10):
            return "Hello World"

        response = self.client.open(path='/paginateroute', method='OPTIONS')
        self.assertEqual(sorted(response.allow), ["GET", "HEAD", "OPTIONS"])

        response = self.client.get(path='/paginateroute/1', method='GET')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(path='/paginateroute/2', method='GET')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(path='/paginateroute/1/5', method='GET')
        self.assertEqual(response.status_code, 200)
