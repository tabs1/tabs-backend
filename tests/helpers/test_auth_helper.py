# testlibraries
from itsdangerous import TimedJSONWebSignatureSerializer as JWT
from itsdangerous import SignatureExpired, BadSignature

from tests.utils.helper import *

def make_token_data(user):
    return {
        'id': user.uuid,
        'username': user.username,
    }

def make_token(user, expires):
    jwt = JWT(app.config['SECRET_KEY'], expires)
    return jwt.dumps(make_token_data(user)).decode('utf-8')


class TestCase(TestBase):

    def setUpClass():
        @app.route("/auth_required", methods=["GET"])
        @Auth.required
        def auth_required():
            return "Hello World"

    ##
    # Auth.required

    def test_auth_required(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        response = self.client.open(path='/auth_required', method='GET', headers={'Authorization': token})
        self.assertEqual(response.status_code, 200)


    def test_auth_required_restricts(self):
        response = self.client.open(path='/auth_required', method='GET')
        self.assertEqual(response.status_code, 401)

        response = self.client.open(path='/auth_required', method='GET')
        self.assertEqual(response.status_code, 401)


    ##
    # generate_token()

    def test_generate_token(self):
        user = UserFactory()
        expires = 100

        token = make_token(user, expires)
        auth_token = Auth.generate_token(user, expires)

        self.assertEqual(token, auth_token)

    ##
    # verify_token

    def test_verify_token(self):
        user = UserFactory()

        verify = Auth.verify_token(make_token(user, 100))
        self.assertDictEqual(verify, make_token_data(user))


    def test_verify_token_bad_signature(self):
        user = UserFactory()
        token = make_token(user, 100)

        app.config['SECRET_KEY'] = 'not-the-same'

        verify = Auth.verify_token(token)
        self.assertFalse(verify)


    def test_verify_token_expires(self):
        user = UserFactory()
        token = make_token(user, -1)

        verify = Auth.verify_token(token)
        self.assertFalse(verify)
