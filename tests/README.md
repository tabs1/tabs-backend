
# Testing Environment
`tabs-backend` contains unit tests and integrations tests in the folder `tests`.
Here are install instructions to setup your environment for running the
tests locally.

The tests will scaffold and tear down the database at the start/end of the tests. 
So this whole suite should only require a database cconnection and the database without
tables to have access to the app.

## Database Dependencies
- Install PostgreSQL 
  - *Linux*
    - `sudo apt-get install postgresql`
    - `sudo systemctl start postgresql`
  - *Mac*
    - `brew install postgresql`
  
- Run the following commands to create a root user in the postgres
database (useful if you are running the tests as root)
    ```
    sudo -u postgres psql postgres
    postgres=# CREATE USER root;
    postgres=# CREATE DATABASE tabs_testing
    postgres=# \connect tabs_testing
    postgres=# CREATE EXTENSION IF NOT EXISTS "uuid-ossp;
    ```

## Python Dependencies

### Environment Dependant
Postgres binaries and Python virtual environment
- `sudo apt install -y libpq-dev python3-venv`

### Flask App Required
- `python3 -m pip install -r installation/dependencies.txt`
- `python3 -m pip install -r tests/dependencies.txt`

## How to run
- From the root directory of this repository: `python3 -m pytest`
- If the front end is not compiled [yarn](https://https://gitlab.com/stevewillson/tabs-backend/-/blob/master/installation/README.md#yarn-httpsyarnpkgcom)
there will be a ui test fail. You can try:
  - Running yarn to build the environment
  - try the test script `tests/start.sh` 
  - YMMV

## Dealing with random fails based on data
There may be sporatic failures based on a poorly formed generation of data. These
are a pain to fix as the data generation is based on randomization. Each test suite uses
a shared [SetUp Method](https://gitlab.com/stevewillson/tabs-backend/-/blob/master/tests/utils/base_test_class.py). 
When a test fails, it will print out the random seed. If you manually change the tests to use this same seed, it 
should re-produce the same data generation, and you can troubleshoot more accurately how to fix the data
generation issue that is causing these errors.

## To run a single test suite 
- `python3 -m pytest tests/controllers/test_device_controller.py`

## To run specific tests [advanced]
- https://docs.pytest.org/en/latest/example/markers.html
- Label the tests with `@pytest.mark.smoke`
- Run: `python3 -m pytest -m smoke`
