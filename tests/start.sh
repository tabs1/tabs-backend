#!/bin/bash

cd application/frontend
pwd
yarn install
yarn build
cd ../..
pwd
python3 -m pytest
