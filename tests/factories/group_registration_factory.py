from tests.utils.helper import *

class GroupRegistrationFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Group
        sqlalchemy_session = db.session


    # Fields
    name = factory.Iterator(['Mufasa', 'Simba', 'Palpatine', 'Luigi', 'Zelda'])
    role = factory.Iterator(['vetting'])
