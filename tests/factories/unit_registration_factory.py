from tests.utils.helper import *

class UnitRegistrationFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Unit
        sqlalchemy_session = db.session

    # Fields
    name = factory.Iterator(['Mufasa', 'Simba', 'Palpatine', 'Luigi', 'Zelda'])
    poc = factory.Iterator(['test@mail.mil'])
