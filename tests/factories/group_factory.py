from tests.utils.helper import *

class GroupFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Group
        sqlalchemy_session = db.session
        exclude = ['_created_at']

    _created_at = factory.fuzzy.FuzzyDateTime(datetime.datetime(2008, 1, 1, tzinfo=datetime.timezone.utc))


    # Fields

    created_at = _created_at
    uuid = factory.Faker('uuid4')
    name = factory.Iterator(['Mufasa', 'Simba', 'Palpatine', 'Luigi', 'Zelda'])
    role = factory.Iterator(['Write', 'Read', 'Palpatine', 'Luigi', 'Zelda'])
