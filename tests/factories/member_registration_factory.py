from tests.utils.helper import *

class MemberRegistrationFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Member
        sqlalchemy_session = db.session
        exclude = ['_test_strings', '_fuzzy_date_time']

    _test_strings = ['Mufasa', 'Simba', 'Palpatine', 'Luigi', 'Zelda']
    _fuzzy_date_time = factory.fuzzy.FuzzyDateTime(datetime.datetime(2008, 1, 1, tzinfo=datetime.timezone.utc))


    # Basic Fields
    roster_num = factory.Iterator(_test_strings)
    analysis_date = factory.Iterator(_test_strings)
    given_name = factory.Iterator(_test_strings)
    status = factory.Iterator(_test_strings)
    duty_status = factory.Iterator(_test_strings)
    # date_of_birth = _fuzzy_date_time
    rank = factory.Iterator(_test_strings)
    duty_position = factory.Iterator(_test_strings)
    coy =  factory.Iterator(_test_strings)

    # ID Fields
    ana_id = factory.Iterator(_test_strings)
    taskera = factory.Iterator(_test_strings)
    bid = factory.Iterator(_test_strings)

    # Origin Fields
    fathers_name = factory.Iterator(_test_strings)
    ethnicity = factory.Iterator(_test_strings)
    home_province = factory.Iterator(_test_strings)
    home_district = factory.Iterator(_test_strings)
    home_village = factory.Iterator(_test_strings)
    birth_province = factory.Iterator(_test_strings)
    birth_district = factory.Iterator(_test_strings)
    birth_village = factory.Iterator(_test_strings)

    # Cell Phone Data
    ### This should probably eventually be its own model ###
    cell_number_1 = factory.Iterator(_test_strings)
    # cn1_date_reported = _fuzzy_date_time
    cell_number_2 = factory.Iterator(_test_strings)
    # cn2_date_reported = _fuzzy_date_time

    # Vetting Fields
    social_media_account1 = factory.Iterator(_test_strings)
    social_media_account2 = factory.Iterator(_test_strings)
    ahmrs = factory.Iterator(_test_strings)
    sofex_case_number = factory.Iterator(_test_strings)
    # date_last_enrolled = _fuzzy_date_time
    # date_screened = _fuzzy_date_time
    recommend = factory.Iterator(_test_strings)
    # revetting_date = _fuzzy_date_time
    actions_taken = factory.Iterator(_test_strings)
