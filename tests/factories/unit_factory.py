from tests.utils.helper import *

class UnitFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Unit
        sqlalchemy_session = db.session
        exclude = ['_created_at']

    _created_at = factory.fuzzy.FuzzyDateTime(datetime.datetime(2008, 1, 1, tzinfo=datetime.timezone.utc))


    # Fields
    created_at = _created_at
    uuid = factory.Faker('uuid4')
    name = factory.Iterator(['Mufasa', 'Simba', 'Palpatine', 'Luigi', 'Zelda'])
    poc = factory.Iterator(['test@mail.mil'])
