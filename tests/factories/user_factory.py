from tests.utils.helper import *

class UserFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = User
        sqlalchemy_session = db.session
        exclude = ['_chars', '_password', '_password_constraint']

    _chars = app.config['ALLOWED_PASSWORD_CHARACTERS']
    _password = factory.fuzzy.FuzzyText(length=20, chars=_chars)
    _password_constraint = factory.LazyAttribute(lambda p: 'Dd$1{}'.format(p._password))

    # Fields
    uuid = factory.Faker('uuid4')
    created_at = factory.fuzzy.FuzzyDateTime(datetime.datetime(2008, 1, 1, tzinfo=datetime.timezone.utc))

    username = factory.Sequence(lambda n: 'username_{}'.format(n))
    password = UserSerializer.hash_password(str(_password_constraint))
    group_admin = factory.fuzzy.FuzzyChoice([True, False])
    is_active = factory.fuzzy.FuzzyChoice([True, False])
    write_privileges = factory.fuzzy.FuzzyChoice([True, False])
    
