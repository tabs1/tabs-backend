from tests.utils.helper import *

class UserRegistrationFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = User
        sqlalchemy_session = db.session
        exclude = ['_chars', '_password']

    _chars = app.config['ALLOWED_PASSWORD_CHARACTERS']
    _password = factory.fuzzy.FuzzyText(length=20, chars=_chars)


    # Fields
    username = factory.Sequence(lambda n: 'username_r{}'.format(n))
    password = factory.LazyAttribute(lambda p: 'Dd$1{}'.format(p._password))
    group_admin = factory.fuzzy.FuzzyChoice([True, False])
    write_privileges = factory.fuzzy.FuzzyChoice([True, False])
    is_active = factory.fuzzy.FuzzyChoice([True, False])

    relationships = {}
