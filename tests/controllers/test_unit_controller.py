from tests.utils.helper import *


class TestCase(TestBase):

    ##
    # /user

    def test_get_unit(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        unit = UnitFactory()
        response =self.client.get(path='/unit/' + unit.uuid,
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})
        logging.debug(response)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['uuid'], unit.uuid)


    def test_get_unit_not_in_db(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        response =self.client.get(path='/unit/' + make_fake_uuid(),
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 404)
        self.assertEqual({'resource' : 'Unit not found'}, response.json['errors'])


    def test_get_unit_requires_auth(self):
        response =self.client.get(path='/unit/fake',
                                method='GET',
                                content_type="application/json")
        logging.debug(response)
        self.assertEqual(response.status_code, 401)

    def test_unit_post(self):
        current_user = UserFactory(is_active=True)
        self.assertEqual(User.query.count(), 1)

        uuid = current_user.uuid
        token = Auth.generate_token(current_user)
        registration = factory.build(dict, FACTORY_CLASS=UnitRegistrationFactory)

        with app.app_context():
            response = self.client.post(path='/unit',
                                 method='POST',
                                 data=json.dumps(registration),
                                 content_type="application/json",
                                 headers={'Authorization': token}).json
            current_uuid = str(g.current_user.uuid)

        logging.debug(response)
        self.assertEqual(uuid, current_uuid)
        self.assertEqual(Unit.query.count(), 1)


    def test_unit_post_missing_data(self):

        registration = factory.build(dict, FACTORY_CLASS=UnitRegistrationFactory)
        registration.pop('name')
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/unit',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        logging.debug(response)
        self.assertEqual(User.query.count(), 0)


    def test_unit_post_disabled_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=UnitRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        app.config['AUTH_ENABLED'] = False
        response = self.client.post(path='/unit',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        logging.debug(response)
        app.config['AUTH_ENABLED'] = True

        self.assertEqual(Unit.query.count(), 1)


    def test_unit_post_requires_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=UnitRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/unit',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json")
        logging.debug(response)
        self.assertEqual(response.status_code, 401)


    def test_unit_post_disabled_account(self):
        current_user = UserFactory(is_active=False)
        uuid = current_user.uuid
        token = Auth.generate_token(current_user)

        registration = factory.build(dict, FACTORY_CLASS=UnitRegistrationFactory)
        self.assertEqual(User.query.count(), 1)

        response = self.client.post(path='/unit',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json",
                             headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 401)


    def test_post_unit_uuid(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        defaults = { 'name' : 'Jar-Jar-Binks' }

        unit = UnitFactory(**defaults)
        uuid = str(unit.uuid)

        response = self.client.post(path='/unit/' + uuid,
                                  method='POST',
                                  data=json.dumps({'name' : 'LukeSkywalker'}),
                                  content_type="application/json",
                                  headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 200)

        unit = Unit.query.get(uuid)
        self.assertEqual(unit.name, 'LukeSkywalker')

    def test_post_unit_uuid_requires_auth(self):
        response =self.client.post(path='/unit/fake',
                                  method='POST',
                                  data=json.dumps({}),
                                  content_type="application/json")
        logging.debug(response)

        self.assertEqual(response.status_code, 401)


    ##
    # /units

    def test_get_units(self):
        count = app.config['RESULTS_PER_PAGE']
        units = UnitFactory.create_batch(count + 1, created_at=datetime.datetime.utcnow())

        current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
        token = Auth.generate_token(current_user)

        response = self.client.get(path='/units',
                                  method='GET',
                                  content_type="application/json",
                                  headers={'Authorization': token}).json
        logging.debug(response)
        self.assertEqual(len(response['units']), count)
        self.assertEqual(response['units'][0]['uuid'], units[0].uuid)
        self.assertEqual(response['units'][count-1]['uuid'], units[count-1].uuid)

        def test_get_units_honors_filters(self):
            unit_1 = UnitFactory(name='DarthSiddeous')
            unit_2 = UnitFactory(name='SenorDarth')
            unit_3 = UnitFactory(name='Ted')


            current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
            token = Auth.generate_token(current_user)

            pattern = {'name' : '%Darth%'}

            response =self.client.get(path='/units?pattern=' + json.dumps(pattern),
                                      method='GET',
                                      content_type="application/json",
                                      headers={'Authorization': token}).json

            self.assertEqual(len(response['units']), 2)
            self.assertEqual(response['units'][0]['uuid'], str(units.uuid))
