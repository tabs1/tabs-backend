from tests.utils.helper import *


class TestCase(TestBase):

    ##
    # /user

    def test_get_user(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        user = UserFactory()
        response =self.client.get(path='/user/' + user.uuid,
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['uuid'], user.uuid)


    def test_get_user_not_in_db(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        response =self.client.get(path='/user/' + make_fake_uuid(),
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})

        self.assertEqual(response.status_code, 404)
        self.assertEqual({'resource' : 'User not found'}, response.json['errors'])


    def test_get_user_requires_auth(self):
        response =self.client.get(path='/user/fake',
                                method='GET',
                                content_type="application/json")

        self.assertEqual(response.status_code, 401)

    def test_user_post(self):
        current_user = UserFactory(is_active=True)
        self.assertEqual(User.query.count(), 1)

        uuid = current_user.uuid
        token = Auth.generate_token(current_user)
        registration = factory.build(dict, FACTORY_CLASS=UserRegistrationFactory)

        with app.app_context():
            response = self.client.post(path='/user',
                                 method='POST',
                                 data=json.dumps(registration),
                                 content_type="application/json",
                                 headers={'Authorization': token}).json
            current_uuid = str(g.current_user.uuid)

        self.assertEqual(uuid, current_uuid)
        self.assertEqual(User.query.count(), 2)


    def test_user_post_missing_data(self):

        registration = factory.build(dict, FACTORY_CLASS=UserRegistrationFactory)
        registration.pop('username')
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/user',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json

        self.assertEqual(User.query.count(), 0)


    def test_user_post_disabled_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=UserRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        app.config['AUTH_ENABLED'] = False
        response = self.client.post(path='/user',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        app.config['AUTH_ENABLED'] = True

        self.assertEqual(User.query.count(), 1)


    def test_user_post_requires_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=UserRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/user',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json")

        self.assertEqual(response.status_code, 401)


    def test_user_post_disabled_account(self):
        current_user = UserFactory(is_active=False)
        uuid = current_user.uuid
        token = Auth.generate_token(current_user)

        registration = factory.build(dict, FACTORY_CLASS=UserRegistrationFactory)
        self.assertEqual(User.query.count(), 1)

        response = self.client.post(path='/user',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json",
                             headers={'Authorization': token})

        self.assertEqual(response.status_code, 401)


    def test_post_user_uuid(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        defaults = { 'username' : 'Jar-Jar-Binks' }

        user = UserFactory(**defaults)
        uuid = str(user.uuid)
        user = User.query.get(uuid)

        response = self.client.post(path='/user/' + uuid,
                                  method='POST',
                                  data=json.dumps({'username' : 'LukeSkywalker',
                                                    'relationships' : {}}),
                                  content_type="application/json",
                                  headers={'Authorization': token})

        self.assertEqual(response.status_code, 200)

        user = User.query.get(uuid)
        self.assertEqual(user.username, 'LukeSkywalker')

    def test_post_user_uuid_requires_auth(self):
        response =self.client.post(path='/user/fake',
                                  method='POST',
                                  data=json.dumps({}),
                                  content_type="application/json")

        self.assertEqual(response.status_code, 401)


    ##
    # /users

    def test_get_users(self):
        count = app.config['RESULTS_PER_PAGE']
        users = UserFactory.create_batch(count + 1, created_at=datetime.datetime.utcnow())

        current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
        token = Auth.generate_token(current_user)

        response =self.client.get(path='/users',
                                  method='GET',
                                  content_type="application/json",
                                  headers={'Authorization': token}).json

        self.assertEqual(len(response['users']), count)
        self.assertEqual(response['users'][0]['uuid'], users[0].uuid)
        self.assertEqual(response['users'][9]['uuid'], users[9].uuid)

    def test_get_users_honors_filters(self):
        user = UserFactory(is_active=False)

        current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
        token = Auth.generate_token(current_user)

        filter = {'is_active':False}

        response =self.client.get(path='/users?filter=' + json.dumps(filter),
                                  method='GET',
                                  content_type="application/json",
                                  headers={'Authorization': token}).json

        self.assertEqual(len(response['users']), 1)
        self.assertEqual(response['users'][0]['uuid'], str(user.uuid))



    ##
    # /user/token

    def test_user_token(self):
        password = '1234'
        hash = UserSerializer.hash_password(password)
        user = UserFactory(password=hash)

        login_data = {
            'password': password,
            'username': user.username,
        }

        with patch.object(Auth, 'generate_token', return_value='TOKEN') as mock_auth:
            with patch.object(User, 'get_and_validate', return_value=user) as mock_get_user:
                data = self.client.post(path='/user/token',
                             method='POST',
                             data=json.dumps(login_data),
                             content_type="application/json").json

            mock_get_user.assert_called_once_with(user.username, password)
            mock_auth.assert_called_once_with(user)
            self.assertDictEqual({'token':'TOKEN'}, data)


    def test_user_token_invalid_user(self):
        password = '1234'
        hash = UserSerializer.hash_password(password)
        user = UserFactory(password=hash)

        login_data = {
            'password': 'wrong-password',
            'username': user.username,
        }

        response = self.client.post(path='/user/token',
                     method='POST',
                     data=json.dumps(login_data),
                     content_type="application/json").json


        self.assertDictEqual({'error':True}, response)


    #
    ## /user/session

    def test_user_session(self):
        user = UserFactory(is_active=True)
        token = Auth.generate_token(user)

        response = self.client.post(path='/user/session',
                     method='POST',
                     data=json.dumps({"token":token}),
                     content_type="application/json")

        self.assertEqual(response.status_code, 200)


    def test_user_session_expired(self):
        user = UserFactory(is_active=True)
        token = Auth.generate_token(user, expiration=-1)

        response = self.client.post(path='/user/session',
                     method='POST',
                     data=json.dumps({"token":token}),
                     content_type="application/json")

        self.assertEqual(response.status_code, 403)


    def test_user_session_invalid_signature(self):
        user = UserFactory(is_active=True)

        current_key = app.config['SECRET_KEY']
        app.config['SECRET_KEY'] = 'new_key'

        token = Auth.generate_token(user)
        app.config['SECRET_KEY'] = current_key

        response = self.client.post(path='/user/session',
                     method='POST',
                     data=json.dumps({"token":token}),
                     content_type="application/json")

        self.assertEqual(response.status_code, 403)


    def test_user_disabled(self):
        user = UserFactory(is_active=False)
        token = Auth.generate_token(user, expiration=-1)

        response = self.client.post(path='/user/session',
                     method='POST',
                     data=json.dumps({"token":token}),
                     content_type="application/json")

        self.assertEqual(response.status_code, 403)
