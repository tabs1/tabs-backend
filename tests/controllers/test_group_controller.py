from tests.utils.helper import *


class TestCase(TestBase):

    ##
    # /user

    def test_get_group(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        group = GroupFactory()
        response =self.client.get(path='/group/' + group.uuid,
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})
        logging.debug(response)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['uuid'], group.uuid)


    def test_get_group_not_in_db(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        response =self.client.get(path='/group/' + make_fake_uuid(),
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 404)
        self.assertEqual({'resource' : 'Group not found'}, response.json['errors'])


    def test_get_group_requires_auth(self):
        response =self.client.get(path='/group/fake',
                                method='GET',
                                content_type="application/json")
        logging.debug(response)
        self.assertEqual(response.status_code, 401)

    def test_group_post(self):
        current_user = UserFactory(is_active=True)
        self.assertEqual(User.query.count(), 1)

        uuid = current_user.uuid
        token = Auth.generate_token(current_user)
        registration = factory.build(dict, FACTORY_CLASS=GroupRegistrationFactory)

        with app.app_context():
            response = self.client.post(path='/group',
                                 method='POST',
                                 data=json.dumps(registration),
                                 content_type="application/json",
                                 headers={'Authorization': token}).json
            current_uuid = str(g.current_user.uuid)

        logging.debug(response)
        self.assertEqual(uuid, current_uuid)
        self.assertEqual(Group.query.count(), 1)


    def test_group_post_missing_data(self):

        registration = factory.build(dict, FACTORY_CLASS=GroupRegistrationFactory)
        registration.pop('role')
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/group',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        logging.debug(response)
        self.assertEqual(User.query.count(), 0)


    def test_group_post_disabled_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=GroupRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        app.config['AUTH_ENABLED'] = False
        response = self.client.post(path='/group',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        logging.debug(response)
        app.config['AUTH_ENABLED'] = True

        self.assertEqual(Group.query.count(), 1)


    def test_group_post_requires_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=GroupRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/group',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json")
        logging.debug(response)
        self.assertEqual(response.status_code, 401)


    def test_group_post_disabled_account(self):
        current_user = UserFactory(is_active=False)
        uuid = current_user.uuid
        token = Auth.generate_token(current_user)

        registration = factory.build(dict, FACTORY_CLASS=GroupRegistrationFactory)
        self.assertEqual(User.query.count(), 1)

        response = self.client.post(path='/group',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json",
                             headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 401)


    def test_post_group_uuid(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        defaults = { 'name' : 'Jar-Jar-Binks' }

        group = GroupFactory(**defaults)
        uuid = str(group.uuid)

        response = self.client.post(path='/group/' + uuid,
                                  method='POST',
                                  data=json.dumps({'name' : 'LukeSkywalker',
                                                   'relationships': {}}),
                                  content_type="application/json",
                                  headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 200)

        group = Group.query.get(uuid)
        self.assertEqual(group.name, 'LukeSkywalker')

    def test_post_group_uuid_requires_auth(self):
        response =self.client.post(path='/group/fake',
                                  method='POST',
                                  data=json.dumps({}),
                                  content_type="application/json")
        logging.debug(response)

        self.assertEqual(response.status_code, 401)


    ##
    # /groups

    def test_get_groups(self):
        count = app.config['RESULTS_PER_PAGE']
        groups = GroupFactory.create_batch(count + 1, created_at=datetime.datetime.utcnow())

        current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
        token = Auth.generate_token(current_user)

        response = self.client.get(path='/groups',
                                  method='GET',
                                  content_type="application/json",
                                  headers={'Authorization': token}).json
        logging.debug(response)
        self.assertEqual(len(response['groups']), count)
        self.assertEqual(response['groups'][0]['uuid'], groups[0].uuid)
        self.assertEqual(response['groups'][count-1]['uuid'], groups[count-1].uuid)

        def test_get_groups_honors_filters(self):
            group_1 = GroupFactory(name='DarthSiddeous')
            group_2 = GroupFactory(name='SenorDarth')
            group_3 = GroupFactory(name='Ted')


            current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
            token = Auth.generate_token(current_user)

            pattern = {'name' : '%Darth%'}

            response =self.client.get(path='/groups?pattern=' + json.dumps(pattern),
                                      method='GET',
                                      content_type="application/json",
                                      headers={'Authorization': token}).json

            self.assertEqual(len(response['groups']), 2)
            self.assertEqual(response['groups'][0]['uuid'], str(groups.uuid))
