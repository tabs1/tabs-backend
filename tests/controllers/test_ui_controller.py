from tests.utils.helper import *

class TestCase(TestBase):


    def test_get_root(self):
        response = self.client.get(path='/',
                                method='GET',
                                content_type="application/json")

        self.assertEqual(response.status_code, 200)


    def test_get_generic_path(self):
        response = self.client.get(path='/whatever',
                                method='GET',
                                content_type="application/json")

        self.assertEqual(response.status_code, 404)


    def test_get_admin(self):
        response = self.client.get(path='/admin',
                                method='GET',
                                content_type="application/json")

        self.assertEqual(response.status_code, 200)


    def test_get_admin__with_path(self):
        response = self.client.get(path='/admin/whatever',
                                method='GET',
                                content_type="application/json")

        self.assertEqual(response.status_code, 200)
