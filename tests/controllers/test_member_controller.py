from tests.utils.helper import *


class TestCase(TestBase):

    ##
    # /user

    def test_get_member(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        member = MemberFactory()
        response =self.client.get(path='/member/' + member.uuid,
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})
        logging.debug(response)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['uuid'], member.uuid)


    def test_get_member_not_in_db(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        response =self.client.get(path='/member/' + make_fake_uuid(),
                                method='GET',
                                content_type="application/json",
                                headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 404)
        self.assertEqual({'resource' : 'Member not found'}, response.json['errors'])


    def test_get_member_requires_auth(self):
        response =self.client.get(path='/member/fake',
                                method='GET',
                                content_type="application/json")
        logging.debug(response)
        self.assertEqual(response.status_code, 401)

    def test_member_post(self):
        current_user = UserFactory(is_active=True)
        self.assertEqual(User.query.count(), 1)

        uuid = current_user.uuid
        token = Auth.generate_token(current_user)
        registration = factory.build(dict, FACTORY_CLASS=MemberRegistrationFactory)

        with app.app_context():
            response = self.client.post(path='/member',
                                 method='POST',
                                 data=json.dumps(registration),
                                 content_type="application/json",
                                 headers={'Authorization': token}).json
            current_uuid = str(g.current_user.uuid)

        logging.debug(response)
        self.assertEqual(uuid, current_uuid)
        self.assertEqual(Member.query.count(), 1)


    def test_member_post_missing_data(self):

        registration = factory.build(dict, FACTORY_CLASS=MemberRegistrationFactory)
        registration.pop('roster_num')
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/member',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        logging.debug(response)
        self.assertEqual(User.query.count(), 0)


    def test_member_post_disabled_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=MemberRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        app.config['AUTH_ENABLED'] = False
        response = self.client.post(path='/member',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json").json
        logging.debug(response)
        app.config['AUTH_ENABLED'] = True

        self.assertEqual(Member.query.count(), 1)


    def test_member_post_requires_auth(self):

        registration = factory.build(dict, FACTORY_CLASS=MemberRegistrationFactory)
        self.assertEqual(User.query.count(), 0)

        response = self.client.post(path='/member',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json")
        logging.debug(response)
        self.assertEqual(response.status_code, 401)


    def test_member_post_disabled_account(self):
        current_user = UserFactory(is_active=False)
        uuid = current_user.uuid
        token = Auth.generate_token(current_user)

        registration = factory.build(dict, FACTORY_CLASS=MemberRegistrationFactory)
        self.assertEqual(User.query.count(), 1)

        response = self.client.post(path='/member',
                             method='POST',
                             data=json.dumps(registration),
                             content_type="application/json",
                             headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 401)


    def test_post_member_uuid(self):
        current_user = UserFactory(is_active=True)
        token = Auth.generate_token(current_user)

        defaults = { 'given_name' : 'Jar-Jar-Binks' }

        member = MemberFactory(**defaults)
        uuid = str(member.uuid)

        response = self.client.post(path='/member/' + uuid,
                                  method='POST',
                                  data=json.dumps({'given_name' : 'LukeSkywalker'}),
                                  content_type="application/json",
                                  headers={'Authorization': token})
        logging.debug(response)
        self.assertEqual(response.status_code, 200)

        member = Member.query.get(uuid)
        self.assertEqual(member.given_name, 'LukeSkywalker')

    def test_post_member_uuid_requires_auth(self):
        response =self.client.post(path='/member/fake',
                                  method='POST',
                                  data=json.dumps({}),
                                  content_type="application/json")
        logging.debug(response)

        self.assertEqual(response.status_code, 401)


    ##
    # /members

    def test_get_members(self):
        count = app.config['RESULTS_PER_PAGE']
        members = MemberFactory.create_batch(count + 1, created_at=datetime.datetime.utcnow())

        current_user = UserFactory(is_active=True, created_at=datetime.datetime.utcnow())
        token = Auth.generate_token(current_user)

        response = self.client.get(path='/members',
                                  method='GET',
                                  content_type="application/json",
                                  headers={'Authorization': token}).json
        logging.debug(response)
        self.assertEqual(len(response['members']), count)
        self.assertEqual(response['members'][0]['uuid'], members[0].uuid)
        self.assertEqual(response['members'][count-1]['uuid'], members[count-1].uuid)
