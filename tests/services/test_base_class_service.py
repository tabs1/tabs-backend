from tests.utils.helper import *


class TestCase(TestBase):

    ##
    # create()

    def test_create(self):
        registration = make_unit_registration()
        unit = Unit.query.first()
        self.assertEqual(Unit.query.count(), 0)

        unit_service = UnitService().create(registration)
        unit = unit_service.instance
        self.assertEqual(Unit.query.count(), 1)

        self.assertTrue(unit)
        self.assertEqual(unit_service.instance, unit)

        unit_data = {
            field: getattr(unit, field)
            for field in registration.keys()
        }
        logging.debug(registration)
        logging.debug(cast_dict(unit_data))

        self.assertDictEqual(registration, cast_dict(unit_data))


    def test_create_fail_missing_field(self):
        registration = make_unit_registration()
        logging.debug(registration)
        registration.pop('name')
        keys = list(registration)
        random.shuffle(keys)
        registration.pop(keys[0])

        unit_service = UnitService().create(registration)

        self.assertFalse(unit_service.instance)
        self.assertTrue(unit_service.errors)
        self.assertEqual(Unit.query.count(), 0)


    def test_create_fail_bad_value(self):
        registration = make_user_registration()
        registration['username'] = 4

        user_service = UserService().create(registration)

        self.assertFalse(user_service.instance)
        self.assertTrue(user_service.errors)
        self.assertEqual(User.query.count(), 0)


    def test_update(self):
        unit = UnitFactory()
        unit_service = UnitService(unit).update({ 'name' : "hello" })

        unit_update = Unit.query.get(unit.uuid)
        self.assertEqual(unit_update.name, "hello")


    def test_update_ensure_uuid_no_change(self):
        unit = UnitFactory()
        uuid = str(unit.uuid)
        new_uuid = 'eefd1a8a-abf9-47a4-b945-34fd45e45621'
        unit_service = UnitService(unit).update({ 'uuid': new_uuid })

        unit_update = Unit.query.get(new_uuid)
        self.assertFalse(unit_update)

        unit = Unit.query.get(uuid)
        self.assertTrue(unit)


    def test_update_fail_null_value(self):
        unit = UnitFactory()
        uuid = unit.uuid
        unit_service = UnitService(unit).update({ 'name' : None })

        self.assertTrue(unit_service.errors)

        unit_update = Unit.query.get(uuid)
        self.assertEqual(unit_update.name, unit.name)


    def test_update_fail_bad_value(self):
        unit = UnitFactory()
        uuid = unit.uuid
        unit_service = UnitService(unit).update({ 'name' : 5 })

        self.assertTrue(unit_service.errors)

        unit_update = Unit.query.get(uuid)
        self.assertEqual(unit_update.name, unit.name)


    ##
    # delete()


    def test_delete(self):
        unit = UnitFactory()
        db.session.commit()
        uuid = unit.uuid
        unit_service = UnitService(unit).delete()

        self.assertFalse(Unit.query.get(uuid))


    def test_delete__wont_delete_with_associations(self):
        unit = UnitFactory()
        uuid = str(unit.uuid)

        unit_service = UnitService(unit).append('groups', GroupService(GroupFactory()))
        unit_service.delete()

        self.assertTrue(Unit.query.get(uuid))
        self.assertTrue(unit_service.errors)



    ##
    # append()

    def test_append(self):
        unit = UnitFactory()
        group_service = GroupService(GroupFactory())

        self.assertEqual(GroupUnit.query.count(), 0)
        unit_service = UnitService(unit).append('groups', group_service)
        self.assertEqual(len(unit.groups), 1)
        self.assertEqual(Unit.query.count(), 1)


    def test_append_associates_once(self):
        unit = UnitFactory()
        group_service = GroupService(GroupFactory())

        unit.groups.append(group_service.instance)

        db.session.add(unit)
        db.session.commit()

        self.assertEqual(GroupUnit.query.count(), 1)
        unit_service = UnitService(unit).append('groups', group_service)
        self.assertEqual(len(unit.groups), 1)
        self.assertEqual(GroupUnit.query.count(), 1)


    def test_append_allows_multiple_associations(self):
        unit = UnitFactory()

        unit_service = UnitService(unit).append('groups', GroupService(GroupFactory()))
        self.assertEqual(len(unit.groups), 1)

        unit_service = unit_service.append('groups', GroupService(GroupFactory()))
        self.assertEqual(len(unit.groups), 2)


    def test_append_properly_associates(self):
        unit = UnitFactory()
        unit_2 = UnitFactory()
        group = GroupFactory()

        unit_2.groups.append(group)

        db.session.add(unit_2)
        db.session.commit()

        self.assertEqual(GroupUnit.query.count(), 1)
        unit_service = UnitService(unit).append('groups', GroupService(group))
        self.assertEqual(len(unit.groups), 1)
        self.assertEqual(GroupUnit.query.count(), 2)


    ##
    # remove()

    def test_remove_deletes_association(self):
        unit = UnitFactory()
        group = GroupFactory()
        group_2 = GroupFactory()

        unit_service = UnitService(unit).append('groups', GroupService(group))
        unit_service.append('groups', GroupService(group_2))
        self.assertEqual(len(unit.groups), 2)

        unit_service = unit_service.remove('groups', GroupService(group))
        self.assertEqual(len(unit.groups), 1)
        self.assertEqual(unit.groups[0].uuid, group_2.uuid)
        self.assertEqual(GroupUnit.query.count(), 1)


    ##
    # remove_all()


    def test_remove_all(self):
        unit = UnitFactory()
        uuid = str(unit.uuid)

        unit_2 = UnitFactory()
        uuid_2 = str(unit_2.uuid)

        group = GroupFactory()
        group_2 = GroupFactory()

        unit_service = UnitService(unit).append('groups', GroupService(group))
        unit_service.append('groups', GroupService(group_2))
        self.assertEqual(len(unit.groups), 2)

        unit_service_2 = UnitService(unit_2).append('groups', GroupService(group_2))
        self.assertEqual(len(unit_2.groups), 1)
        self.assertEqual(GroupUnit.query.count(), 3)

        unit_service = unit_service.remove_all('groups')
        self.assertEqual(len(Unit.query.get(uuid).groups), 0)
        self.assertEqual(len(Unit.query.get(uuid_2).groups), 1)
        self.assertEqual(GroupUnit.query.count(), 1)


    ##
    # simple_response()

    def test_simple_response_data(self):
        unit = UnitFactory()
        unit_service = UnitService(unit)
        unit_service.status_code = 900

        with patch.object(UnitSerializer, 'dump', return_value={}) as mocked:
            response, code = unit_service.simple_response()
            mocked.assert_called_once_with(unit)
            self.assertEqual(response, {})
            self.assertEqual(code, 900)


    def test_simple_response_error(self):
        unit_service = UnitService()
        unit_service.errors = 'lots'

        response, code = unit_service.simple_response()
        self.assertDictEqual(response, {'errors' : unit_service.errors})
