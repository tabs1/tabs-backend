from tests.utils.helper import *

class TestCreate(TestBase):
    
    def test_create_pass(self):
        registration = make_user_registration()
        user_service = UserService().create(registration)
        print(user_service.errors) # leave this here, figuring out random fails
        uuid = user_service.instance.uuid
        user = User.query.get(uuid)
        self.assertTrue(user)
        self.assertEqual(registration['username'], user.username)
        self.assertTrue(user.is_active)

        verify = User.get_and_validate(registration['username'], registration['password'])
        self.assertTrue(verify)
    

    def test_create_fail_bad_value(self):
        registration = make_user_registration()
        registration['username'] = "none"

        user_service = UserService().create(registration)

        self.assertEqual(User.query.count(), 0)
        self.assertTrue(user_service.errors)

    def test_create_fail_null_value(self):
        registration = make_user_registration()
        registration['username'] = None

        user_service = UserService().create(registration)

        self.assertEqual(User.query.count(), 0)
        self.assertTrue(user_service.errors)
        

    def test_create_fail_missing_field(self):
        registration = make_user_registration()
        registration.pop('username')

        user_service = UserService().create(registration)

        self.assertEqual(User.query.count(), 0)
        self.assertTrue(user_service.errors)


    def test_create_fail_unqiue_constraint(self):
        ## this test will throw a DB error 
        ## its OK, that is shows up
        user = UserFactory()
        self.assertEqual(User.query.count(), 1)

        db.session.commit()
        registration = make_user_registration({ 'username' : user.username })
        user_service = UserService().create(registration)
        self.assertEqual(db.session.query(User).count(), 1)
        self.assertTrue(user_service.errors)


class TestUpdate(TestBase):

    def test_update_can_properly_update_password(self):
        password = '123qweASDDDSSSSSSSS!@#'
        hash = UserSerializer.hash_password(password)

        username = 'JediKnightAlex'
        user = UserFactory(is_active=True, password=hash, username=username)

        verified = User.get_and_validate(username, password)
        self.assertTrue(verified)

        password = '123qweASDDDSSSSSSSS!@#-2'
        user_service = UserService(verified).update({'password' : password})

        verified = User.get_and_validate(username, password)
        self.assertTrue(verified)


        

