from tests.utils.helper import *

## validators
from application.serializers.user_serializer import username_validator
from application.serializers.user_serializer import password_validator


class TestValidatorUsername(unittest.TestCase):

    def test_attached(self):
        username = UserSerializer().fields["username"]
        self.assertTrue(username.validate == username_validator)

    def test_too_long(self):
        with self.assertRaises(exceptions.ValidationError) as context:
            username_validator("12345678901234567890123456789012345678901234567890123456789012345678901")

    def test_too_short(self):
        with self.assertRaises(exceptions.ValidationError) as context:
            username_validator("12345")

    def test_username_constraints(self):
        self.assertFalse(UserSerializer.username_constraints('*'))
        self.assertFalse(UserSerializer.username_constraints('st*r'))
        self.assertTrue(UserSerializer.username_constraints('a-'))
        self.assertFalse(UserSerializer.username_constraints('-a'))
        self.assertFalse(UserSerializer.username_constraints('_a'))
        self.assertFalse(UserSerializer.username_constraints('spa ce'))


class TestValidatorPassword(unittest.TestCase):

    def test_attached(self):
        password = UserSerializer().fields["password"]
        self.assertEqual(password.validate, password_validator)

    def test_password_too_long(self):
        with self.assertRaises(exceptions.ValidationError) as context:
            password_validator("12345678901234567890123456789012345678901234567890123456789012345678901")

    def test_password_too_short(self):
        with self.assertRaises(exceptions.ValidationError) as context:
            password_validator("12345678")

    def test_password_requirements(self):
        self.assertTrue(UserSerializer.password_requirements('Aa2@'))
        self.assertFalse(UserSerializer.password_requirements('Aa2'))
        self.assertFalse(UserSerializer.password_requirements('Aa@'))
        self.assertFalse(UserSerializer.password_requirements('A2@'))
        self.assertFalse(UserSerializer.password_requirements('a2@'))

    def test_password_constraints(self):
        self.assertFalse(UserSerializer.password_constraints('"'))
        self.assertFalse(UserSerializer.password_constraints('A"'))
        self.assertFalse(UserSerializer.password_constraints('spa ce'))


class TestRequiredFields(unittest.TestCase):
    required = [
        "username",
        "password",
        "is_active",
        "write_privileges",
        "group_admin"
    ]

    def test_required(self):
        for field in UserSerializer().fields:
            requirement = True if field in self.required else False
            validation = UserSerializer().fields[field].required
            self.assertEqual(validation, requirement)

class TestSerializer(unittest.TestCase):

    def test_well_formed_data(self):
        well_formed_data = factory.build(dict, FACTORY_CLASS=UserFactory)
        well_formed_data.pop('password')
        assert UserSerializer(unknown=EXCLUDE, partial=True).load(well_formed_data)
