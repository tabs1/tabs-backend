##!/usr/bin/env bash

# docker should be set up already on the host machiner

export CONTAINER_NAME='pgtest'

echo "[+] Stopping and removing container with name: $CONTAINER_NAME"

docker kill $CONTAINER_NAME
docker container prune --force

export PGPASSWORD=testpass
echo "[+] Setting password to '$PGPASSWORD'"
export PG_HOSTNAME='localhost'
export POSTGRES_PORT='5432'
export POSTGRES_USER='postgres'
export POSTGRES_DB='tabs'
export POSTGRES_DOCKER_CONTAINER='postgres'

echo "[+] Starting the docker container with postgres named $CONTAINER_NAME"

docker run \
    --name $CONTAINER_NAME \
    -e POSTGRES_PASSWORD=$PGPASSWORD \
    -e POSTGRES_DB=$POSTGRES_DB \
    -p $POSTGRES_PORT:5432 \
    -d $POSTGRES_DOCKER_CONTAINER

echo "[+] Waiting for $POSTGRES_DOCKER_CONTAINER docker container to start"
echo "[+] 5"
sleep 1
echo "[+] 4"
sleep 1
echo "[+] 3"
sleep 1
echo "[+] 2"
sleep 1
echo "[+] 1"
sleep 1

echo "[+] Checking connection"

psql -h $PG_HOSTNAME -p $POSTGRES_PORT -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "SELECT 'OK' AS status;"

psql -h $PG_HOSTNAME -p $POSTGRES_PORT -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'

# Install Python env
echo "[+] Installing the python virtual environment"
python3 -m venv env
source env/bin/activate
pip3 install -r ./dependencies.txt

## Build Frontend
cd ./application/frontend
yarn install
yarn build
cd ../..

export DATABASE_URL="postgresql://$POSTGRES_USER:$PGPASSWORD@$PG_HOSTNAME:$POSTGRES_PORT/$POSTGRES_DB"

# make sure uwsgi-plugin-python3 is installed

echo "[+] Installing uwsgi-plugin-python3"

sudo apt-get install -y uwsgi-plugin-python3

## Start flask
export FLASK_APP=launch.py
flask db migrate
flask db upgrade

# only seed the database when testing, we will need to have backup/restore functionality for production
flask dbseed

# uwsgi

sudo cp ./tabs.service /etc/systemd/system/tabs.service

echo "[+] Enabling the tabs service for uWSGI"

sudo systemctl enable tabs.service

echo "[+] Stopping and starting uwsgi to host the project"

sudo systemctl restart tabs.service

# can also use 'flask run' to start a local dev server if uwsgi is not available
#flask run