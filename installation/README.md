# Setup

## Prerequisites

- [virtualenv](https://virtualenv.pypa.io/en/latest/) (If using install script)
- PostgreSQL
- Yarn (front end)

## Yarn https://yarnpkg.com
May be different per environment
- curl -o- -L https://yarnpkg.com/install.sh | bash
- export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

## Postgres Database ***only need to do during initial setup***
- `brew install postgres` to install PostgreSQL
- `brew services start postgresql` to start PostgreSQL now and at login
- `createdb tabs` to create the local development database
- `echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";' | postgres psql tabs_testing`

## Python/Flask

- Run the install.sh script. This will create a virtualenv and install all of the necessary dependencies
- All that should be needed after install should be a flask run command.

## Yarn Frontend Installation
- `cd application/frontend` to move to the react frontend directory
- `yarn install` to install all of the packages needed to build the project
- `yarn build` to build create the static code that will be served by the web app
- `cd ../..` to return to the project root directory

# Running

## Database Migrations

These should be checked with each code pull

### Database Seeds
This is data that is required for the app to run (ie configurations). These should be written in a way
that is idempotent, and their existance is tested before inserting (get_or_create is PERFECT for this)
allowing for the entire seed file to always run without issue.

- Seeds are configured in the /seeds
- Run: `flask dbseed`

### Altering Database

- Make changes to your models
- RUN: `flask db migrate`
- Verify that the created version file makes sense
    - https://flask-migrate.readthedocs.io/en/latest/
    - The migration script needs to be reviewed and edited, as Alembic currently does not detect every change you make to your models. In particular, Alembic is currently unable to detect table name changes, column name changes, or anonymously named constraints. A detailed summary of limitations can be found in the Alembic autogenerate documentation. Once finalized, the migration script also needs to be added to version control.

### Updating Local Database
- RUN: `flask db upgrade`

## Configuring Basic Settings

### Config.py
Many of the main settings are controlled by environment variables referenced in the config.py file in the root directory of the project.
These variables are:
- Basic Config
    -   ENV - defaults to `development`, a WSGI server should be used in production.
    -   SECRET_KEY - defaults to `secret_key`, key that controls token signing. *SHOULD BE CHANGED BEFORE DEPLOYMENT*

- Pagination
    -   RESULTS_PER_PAGE - defaults to 10, controls the number of results per page for the routes where pagination is enabled
    -   MAX_RESULTS_PER_PAGE = defaults to 500, limits the number of results that can be shown on the pages where pagination is enabled. Should generally remain below 500 to maintain usability.

- Database
    -   SQLALCHEMY_DATABASE_URI = defaults to `postgresql://localhost/tabs`, should be set to whatever database is in use.
    -   SQLALCHEMY_TRACK_MODIFICATIONS = defaults to `False`, controls whether the server changes its settings when code changes. Can be changed to true for rapid development and debugging.
    -   DATA_ENDPOINT_URL = defaults to `TODO ??? where is the default set`

- Password Settings
    -   ALLOWED_PASSWORD_CHARACTERS = string.ascii_letters + string.digits + password_punctuation_allowed
    -   ALLOWED_PASSWORD_PUNCTUATION = password_punctuation_allowed
    -   BCRYPT_CYCLES = 12 ### Dont change this after deploy!

- Token Settings
    -   TOKEN_EXPIRATION = defaults to `two_weeks`, controls how long tokens are valid before expiring.
    -   AUTH_ENABLED = defaults to `true` - enables authentication on endpoints and should therefore always be set to true unless



## Launch

### running the front end
This will create the required front end packages

```
cd application/frontend
yarn install
yarn build
cd ../..
```

### start flask
- `export FLASK_APP=launch.py`
- `flask run`
