##!/usr/bin/env bash

# Install Postgres database
brew install postgres
createdb tabs
echo 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"' | psql -d tabs

## Install Python env
python3 -m venv env
source env/bin/activate
pip3 install -r ./dependencies.txt

## Build Frontend
cd ../application/frontend
yarn install
yarn build
cd ../..

## Start flask
export FLASK_APP=launch.py
flask db migrate
flask db upgrade
flask dbseed
flask run
